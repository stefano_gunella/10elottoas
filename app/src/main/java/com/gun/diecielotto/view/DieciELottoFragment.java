package com.gun.diecielotto.view;

import java.util.Calendar;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.NumberPicker;

import com.gun.diecielotto.R;
import android.app.Activity;
import android.app.Fragment;

public class DieciELottoFragment extends Fragment{
	public DatePicker datePicker = null;
	public NumberPicker numberPicker = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.diecielotto_fragment, container, false);
        
		datePicker = (DatePicker)rootView.findViewById(R.id.datePicker);
		datePicker.setMaxDate(Calendar.getInstance().getTimeInMillis());
		numberPicker = (NumberPicker)rootView.findViewById(R.id.numberPicker);
		numberPicker.setMinValue(1);
		numberPicker.setMaxValue(288);

        return rootView;
    }
    
    @Override
    public void onAttach(Activity activity) {
    	// TODO Auto-generated method stub
    	super.onAttach(activity);
    }
}
