package com.gun.diecielotto.view;

import java.util.List;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.gun.diecielotto.DieciELottoActivity;
import com.gun.diecielotto.R;
import com.gun.diecielotto.db.DbManager;
import com.gun.diecielotto.db.GiocateLottoDataItem;
import com.gun.diecielotto.util.GamesUtil;
import com.gun.diecielotto.view.adapter.GamesDataItemAdapter;

public class ShowGamesFragment extends Fragment{
	ListView listViewGames = null;
	View rootView = null;
	final String TAG = "ShowGamesFragment";
	public GamesDataItemAdapter gameDataAdapter = null;
	public List<GiocateLottoDataItem> listaGiocate = null;
	
	public static ShowGamesFragment getInstance() {
//		Bundle bundle = new Bundle();
//      bundle.putString("name", name);
//      bundle.putInt("age", age);
        ShowGamesFragment fragment = new ShowGamesFragment();
//      fragment.setArguments(bundle);
        return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		Log.d(TAG, "- onCreateView -");
		rootView = inflater.inflate(R.layout.show_games_fragment, container, false);
		listViewGames = (ListView) rootView.findViewById(R.id.list_view_games);
		DbManager db = DbManager.getInstance(this.getActivity());
		listaGiocate = db.getListGiocate();
		gameDataAdapter = new GamesDataItemAdapter(this.getActivity(),listaGiocate);
		listViewGames.setAdapter(gameDataAdapter);
		updateImportiGiocateVincite();
		rootView.invalidate();
		return rootView;
	}

	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "onActivityCreated - ");
    }
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState - ");
	}

	TextWatcher watcher = null;
	public void updateImportiGiocateVincite(){
		TextView totDaGiocareView = (TextView)rootView.findViewById(R.id.max_giocata_euro);
		if(totDaGiocareView == null){
			return; // impossibile continuare
		}
		if(totDaGiocareView != null && watcher != null){
			totDaGiocareView.removeTextChangedListener(watcher);
		}
		GamesUtil gu = GamesUtil.getInstance(this.getActivity());
		totDaGiocareView.setText(""+gu.getImportoDaGiocare());
		TextView totGiocatoView = (TextView)rootView.findViewById(R.id.label_tot_giocate);
		totGiocatoView.setText(""+gu.getTotaleGiocate());
		TextView totVincitaView = (TextView)rootView.findViewById(R.id.label_tot_vincita);
		totVincitaView.setText(""+gu.getTotaleVincita());
		ImageView lblRicavatoView = (ImageView)rootView.findViewById(R.id.img_ricavato);
		TextView lblTotRicavatoView = (TextView)rootView.findViewById(R.id.label_tot_ricavato);
		if(gu.getTotaleRicavato() >= 0){
			lblTotRicavatoView.setTextColor(getResources().getColor(R.color.green));
			lblRicavatoView.setImageResource(R.drawable.ic_arrow_up);
		}
		else{
			lblTotRicavatoView.setTextColor(getResources().getColor(R.color.red));
			lblRicavatoView.setImageResource(R.drawable.ic_arrow_down);
		}
		lblTotRicavatoView.setText(""+Math.abs(gu.getTotaleRicavato()));
		totDaGiocareView.invalidate();
		totGiocatoView.invalidate();
		totVincitaView.invalidate();
		lblTotRicavatoView.invalidate();
		watcher = new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				int input = s.length()==0 ? 0:Integer.valueOf(""+s);
				GamesUtil.getInstance(ShowGamesFragment.this.getActivity()).setImportoDaGiocare(input);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		};
		totDaGiocareView.addTextChangedListener(watcher);
	}

	@Override
	public void onDetach() {
		DieciELottoActivity a = (DieciELottoActivity)this.getActivity();
		a.flgCalc = false;
		super.onDetach();
	}
}