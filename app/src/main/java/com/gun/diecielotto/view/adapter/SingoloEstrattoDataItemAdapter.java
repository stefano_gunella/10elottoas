package com.gun.diecielotto.view.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gun.diecielotto.R;
import com.gun.diecielotto.engine.data.AmboDataItem;
import com.gun.diecielotto.engine.data.SingoloEstrattoDataItem;
import com.gun.diecielotto.util.DateUtils;

public class SingoloEstrattoDataItemAdapter extends ArrayAdapter<SingoloEstrattoDataItem> {
	private final Context context;
	private final List<SingoloEstrattoDataItem> lvalues;

	public SingoloEstrattoDataItemAdapter(Context context, List<SingoloEstrattoDataItem> lvalues) {
		super(context, R.layout.list_singoli_layout, lvalues);
		this.context = context;
		this.lvalues = lvalues;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
//		int textColor = Color.WHITE;
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.list_singoli_layout, parent, false);
		TextView indexView = (TextView) rowView.findViewById(R.id.index);
		TextView imageView = (TextView) rowView.findViewById(R.id.icon1);
		TextView textView = (TextView) rowView.findViewById(R.id.n_ext);
		// TextView maxEstrattiView = (TextView) rowView.findViewById(R.id.maxEstrattiConsecutivi);
		TextView dtEstrazioneView = (TextView) rowView.findViewById(R.id.dtEstrazione);
		SingoloEstrattoDataItem item = lvalues.get(position);
//		if(item.maxEstrazioniConsecutive > 0){
//			rowView.setBackgroundColor(Color.GREEN);
//			textColor = Color.BLACK;
//		}
		if(item.isUltimaEstrazione){
			rowView.setBackgroundColor(Color.BLACK);
//			textColor = Color.WHITE;
		}
		indexView.setText(""+(position+1));
		imageView.setText(""+item.ext);
		textView.setText(""+item.tot);
		// maxEstrattiView.setTextColor(textColor);
		// maxEstrattiView.setText(""+item.maxEstrazioniConsecutive);
//		dtEstrazioneView.setTextColor(textColor);
		dtEstrazioneView.setText(DateUtils.dateToStringFormatUI(item.dtEstrazione));
		return rowView;
	}
	
	@Override
	public SingoloEstrattoDataItem getItem(int position) {
		return lvalues.get(position);
	}
} 