package com.gun.diecielotto.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.gun.diecielotto.R;

public class ResultView extends SurfaceView {
	private Bitmap bmp;
	private SurfaceHolder holder;
	private Ball[] balls = null;
	private Paint paint = new Paint();

	public ResultView(Context context,AttributeSet set) {
		super(context);
		holder = getHolder(); // recupero il contenitore
		paint.setColor(Color.WHITE);

		holder.addCallback(new SurfaceHolder.Callback() {
			private UpdateThread updateThread;

			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
			}

			@Override
			public void surfaceCreated(SurfaceHolder holder) {
				updateThread = new UpdateThread(ResultView.this);
				updateThread.setRunning(true);
				updateThread.start();
			}

			@Override
			public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
			}
		});
		bmp = BitmapFactory.decodeResource(getResources(), R.drawable.blue_ball);
	}

	protected void onDraw(Canvas canvas) {
		//      canvas.drawColor(Color.BLACK);
		if(balls != null){
			for (Ball ball: balls) {
				canvas.drawBitmap(bmp, 10, 10, null);
				canvas.drawText(ball.text, ball.pos_x, ball.pos_y, paint);
			}
		}
	}

	public void setResult(String[] data){
		balls = new Ball[data.length];
		for (int i =0;i < data.length;i++) {
			balls[i]=new Ball(data[i]);
		}
	}

	class Ball{
		String text;
		int pos_x,pos_y;
		Ball(String text){
			this.text = text;
			pos_x=0;
			pos_y=0;
		}
	}

	class UpdateThread extends Thread{
		static final long FPS = 10;
		private ResultView view;
		private boolean running = false;

		public UpdateThread(ResultView view) {
			this.view = view;
		}

		public void setRunning(boolean run) {
			running = run;
		}

		@SuppressLint("WrongCall")
		@Override
		public void run() {
			long ticksPS = 1000 / FPS;
			long startTime;
			long sleepTime;
			while (running) {
				Canvas canvas = null;
				startTime = System.currentTimeMillis();
				try {
					canvas = view.getHolder().lockCanvas();
					synchronized (view.getHolder()) {
						view.onDraw(canvas);
					}
				}
				finally {
					if (canvas != null) {
						view.getHolder().unlockCanvasAndPost(canvas);
					}
				}
				sleepTime = ticksPS-(System.currentTimeMillis() - startTime);
				try {
					if (sleepTime > 0)
						sleep(sleepTime);
					else
						sleep(10);
				} 
				catch (Exception e) {

				}
			}
		}
	}
}
