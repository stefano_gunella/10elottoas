package com.gun.diecielotto.view;

import java.util.Arrays;
import java.util.List;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gun.diecielotto.DieciELottoActivity;
import com.gun.diecielotto.R;

public class SelectNumbersToPlayFragment extends Fragment{
	final String TAG = "SelectNumbersToPlayFragment";
	DieciELottoActivity main = null;
	View rootView = null;
//	public HashSet<Integer> listaDaGiocare = new HashSet<Integer>();
	protected GridLayout gridNumeriDaGiocare = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		Log.d(TAG, "onCreateView - ");
		main = (DieciELottoActivity)this.getActivity();
		rootView = inflater.inflate(R.layout.select_numbers_to_play_fragment, container, false);
		LinearLayout.LayoutParams layout_lp = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
		rootView.setLayoutParams(layout_lp);
		return rootView;
	}

	@Override
	public void onDestroyView() {
		Log.d(TAG, "onDestroyView - SelectNumbersToPlayFragment");
		super.onDestroyView();
	}
	
	@Override
	public void onDetach() {
		Log.d(TAG, "onDetach - SelectNumbersToPlayFragment");
		super.onDetach();
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		gridNumeriDaGiocare = (GridLayout)rootView.findViewById(R.id.grid_numbers_2_play_buttons);
		updateButtonsListaDaGiocare();
		Log.d(TAG, "onActivityCreated - ");
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Log.d(TAG, "onSaveInstanceState - ");
	}
	
	public void updateButtonsListaDaGiocare() {
		main.runOnUiThread(new Runnable() {
		    public void run() {
		        gridNumeriDaGiocare.removeAllViews();
		        List<Integer> tmp = Arrays.asList(main.listaDaGiocare.toArray(new Integer[0]));
		        java.util.Collections.sort(tmp);
		        for (Integer n : tmp) {
		        	TextView tv = new TextView(SelectNumbersToPlayFragment.this.getActivity());
		        	tv.setOnClickListener(new OnClickListener() {
		        		@Override
		        		public void onClick(View v) {
		        			Log.d(TAG, "onClick:"+v.getId());
		        			((GridLayout)v.getParent()).removeView(v);
		        			String txt = (String) ((TextView)v).getText();
		        			main.listaDaGiocare.remove(Integer.parseInt(txt));
		        		}
		        	});
		        	LayoutParams tlp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		        	tv.setGravity(Gravity.CENTER);
		        	tv.setLayoutParams(tlp);
		        	tv.setBackgroundResource(R.drawable.red_ball);
		        	tv.setTextColor(getResources().getColor(android.R.color.white));
		        	tv.setText(""+n);
		        	gridNumeriDaGiocare.addView(tv);
		        	gridNumeriDaGiocare.refreshDrawableState();
		        }
		    }
		});
	}
}