package com.gun.diecielotto.view.adapter;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gun.diecielotto.R;
import com.gun.diecielotto.db.DbManager;
import com.gun.diecielotto.db.GiocateLottoDataItem;
import com.gun.diecielotto.util.GamesUtil;

public class GamesDataItemAdapter extends ArrayAdapter<GiocateLottoDataItem> {
	final static String TAG = "GamesDataItemAdapter";
	private DbManager db = null;
	private Context context;
	private List<GiocateLottoDataItem> listaGiocate;
	public int vincitaTot = 0;

	public GamesDataItemAdapter(Context context,List<GiocateLottoDataItem> listaGiocate) {
		super(context, R.layout.list_games_layout, listaGiocate);
		this.context = context;
		this.listaGiocate = listaGiocate;
	}

	public void refreshList(){
		Log.d(TAG, "==== refreshList ==== ");
		super.clear();
		this.db = DbManager.getInstance(context);
		this.listaGiocate = db.getListGiocate();
		this.addAll(this.listaGiocate);
		GamesDataItemAdapter.this.notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Log.d(TAG, "==== getView ==== position" + position);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.list_games_layout, parent, false);
		if( this.listaGiocate.size() > 0) {
			GiocateLottoDataItem item = this.listaGiocate.get(position);
			TextView textRow = (TextView) rowView.findViewById(R.id.row);
			textRow.setText("" + (position + 1));

//		TextView textId = (TextView) rowView.findViewById(R.id.id);
//		textId.setText(""+item._id);

			TextView textData = (TextView) rowView.findViewById(R.id.data);
			textData.setText(item.getDateUI());

			GamesUtil gu = GamesUtil.getInstance(context);
			LinearLayout layoutGiocata = (LinearLayout) rowView.findViewById(R.id.layout_giocata);
			for (String numero : item.getNumeriGiocati()) {
				TextView tv = new TextView(rowView.getContext());
//			LayoutParams tlp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				LayoutParams tlp = new LayoutParams(gu.dpToPx(30), gu.dpToPx(30));
				tv.setGravity(Gravity.CENTER);
				tv.setLayoutParams(tlp);
				tv.setBackgroundResource(R.drawable.blue_ball);
				tv.setTextColor(rowView.getContext().getResources().getColor(android.R.color.white));
				tv.setText(numero);
				layoutGiocata.addView(tv);
			}
			TextView textNumeroGiocateXStatistiche = (TextView) rowView.findViewById(R.id.numero_giocate_x_statistica);
			textNumeroGiocateXStatistiche.setText("#" + item.extStat + " GAP:" + item.ext_delta);

			TextView textNumeroGiocateAttesaPrimaVincita = (TextView) rowView.findViewById(R.id.numero_giocate_attesa_prima_vincita);
			textNumeroGiocateAttesaPrimaVincita.setText("P:" + item.ext_diff);
			TextView textNumeroGiocateAttesaMaxVincita = (TextView) rowView.findViewById(R.id.numero_giocate_attesa_max_vincita);
			textNumeroGiocateAttesaMaxVincita.setText("M:" + item.ext_diff_max_win);

			TextView textEstrazione = (TextView) rowView.findViewById(R.id.num_estrazione);
			textEstrazione.setText("PG:" + item.progressivoGiornaliero);
			TextView idEstrazione = (TextView) rowView.findViewById(R.id.id_estrazione);
			idEstrazione.setText("ID:" + item.id_ext);

			TextView textVincita = (TextView) rowView.findViewById(R.id.vincita);
			textVincita.setText("W:" + item.vincitaTot);
			vincitaTot += item.vincitaTot;

			ImageButton buttonDelete = (ImageButton) rowView.findViewById(R.id.button_delete);
			buttonDelete.setOnClickListener(new ButtonListener(position));
		}
		return rowView;
	}
	
	class ButtonListener implements OnClickListener{
		int position = 0;
		public ButtonListener(int position){
			this.position = position;
		}
		
		@Override
		public void onClick(View v){
			String tag = (String)v.getTag();
			if(tag.equalsIgnoreCase("E")){ 		// Elimina
				DbManager db = DbManager.getInstance(GamesDataItemAdapter.this.context);
				GiocateLottoDataItem item = getItem(position);
				db.deleteGame(item._id);
				listaGiocate.remove(position);
				GamesDataItemAdapter.this.notifyDataSetChanged();
			}
//			else if(tag.equalsIgnoreCase("RV")){// Ricalcola vincite
//				v.post(new Runnable() {
//		            @Override
//		            public void run() {
//		            	GiocateLottoDataItem giocata = getItem(position);
//		            	GamesUtil.controllaVincite(context, giocata, true);
//		            	GamesDataItemAdapter.this.notifyDataSetChanged();
//		            	((DieciELottoActivity)context).refreshTotWin();
//		            }
//		        });	
//			}
		} 
	}

	@Override
	public GiocateLottoDataItem getItem(int position) {
		return listaGiocate.get(position);
	}
	
//	public void updateList(List<GiocateLottoDataItem> listaGiocate){
//		this.listaGiocate = listaGiocate;
//	}
} 