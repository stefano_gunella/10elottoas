package com.gun.diecielotto.view;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.gun.diecielotto.DieciELottoActivity;
import com.gun.diecielotto.R;
import com.gun.diecielotto.util.GamesUtil;

@SuppressLint("CommitPrefEdits")
public class AutomaticPlayFragment extends Fragment{
	final String TAG = "AutomaticPlayFragment";
	DieciELottoActivity main = null;
	View rootView = null;

	public Integer extStat  = 0;
	public Integer extDelta = 0;
//	Boolean flgCalc = true;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		Log.d(TAG, "onCreateView - ");
		main = (DieciELottoActivity)this.getActivity();
		final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(main);
		rootView = inflater.inflate(R.layout.automatic_play_fragment, container, false);

		EditText editNumMaxExt = (EditText)rootView.findViewById(R.id.num_max_ext);
		editNumMaxExt.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
				return GamesUtil.getInstance(main).setNumMaxExt(Integer.parseInt(textView.getText().toString()));
			}
		});
		editNumMaxExt.setText(""+GamesUtil.getInstance(main).getNumMaxExt());

		Switch swDeleteMaxPlay = (Switch)rootView.findViewById(R.id.sw_delete_max_play);
		swDeleteMaxPlay.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean value) {
				Editor editor = preferences.edit();
				editor.putBoolean("sw_delete_max_play", value);
				editor.commit();
			}
		});
		swDeleteMaxPlay.setChecked(preferences.getBoolean("sw_delete_max_play", false));

		Switch swDeleteGamesWin = (Switch)rootView.findViewById(R.id.sw_delete_games_win);
		swDeleteGamesWin.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean value) {
				Editor editor = preferences.edit();
				editor.putBoolean("sw_delete_games_win", value);
				editor.commit();
			}
		});
		swDeleteGamesWin.setChecked(preferences.getBoolean("sw_delete_games_win", false));

		Switch swListSingoliAmbi = (Switch)rootView.findViewById(R.id.sw_list_singoli_ambi);
		swListSingoliAmbi.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean value) {
				Editor editor = preferences.edit();
				editor.putBoolean("sw_list_singoli_ambi", value);
				editor.commit();
			}
		});
		swListSingoliAmbi.setChecked(preferences.getBoolean("sw_list_singoli_ambi", false));

		Switch swGiocategruppi = (Switch)rootView.findViewById(R.id.sw_giocate_gruppi);
		swGiocategruppi.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean value) {
				Editor editor = preferences.edit();
				editor.putBoolean("sw_giocate_gruppi", value);
				editor.commit();
			}
		});
		swGiocategruppi.setChecked(preferences.getBoolean("sw_giocate_gruppi", false));

		// =========================================
		// gestisce le checkbox [1-10] delle giocate
		// =========================================
		OnCheckedChangeListener chkListener = new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton checkbox, boolean value) {
				String txt = checkbox.getText().toString();
				_updateChkPreferences(txt, value);
			}
		};

		CheckBox[] chNumeriDaGiocare = new CheckBox[10];
		for (int i = 0; i<10 ; i++) {
			CheckBox item = (CheckBox)rootView.findViewWithTag("ch_"+(i+1));
			item.setChecked(GamesUtil.getInstance(main).isChkSelected(i+1));
			item.setOnCheckedChangeListener(chkListener);
			chNumeriDaGiocare[i]=item;
		}

		EditText editMaiEstratti = (EditText)rootView.findViewById(R.id.ed_n_numeri_meno_estratti);
		editMaiEstratti.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView textView, int arg1, KeyEvent arg2) {
				String sNumMaiEstratti = textView.getText().toString();
				Editor editor = preferences.edit();
				editor.putInt("n_meno_estratti", Integer.parseInt(sNumMaiEstratti));
				editor.commit();
				return true;
			}
		});
		editMaiEstratti.setText(""+preferences.getInt("n_meno_estratti", 0));

		EditText editOffsetClassifica = (EditText)rootView.findViewById(R.id.ed_offset_in_classifica);
		editOffsetClassifica.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView textView, int arg1, KeyEvent arg2) {
				String sOffsetClassifica = textView.getText().toString();
				Editor editor = preferences.edit();
				editor.putInt("n_offset_in_classifica", Integer.parseInt(sOffsetClassifica));
				editor.commit();
				return true;
			}
		});
		editOffsetClassifica.setText(""+preferences.getInt("n_offset_in_classifica", 0));
		return rootView;
	}

	private void _updateChkPreferences(String txt, boolean value) {
		StringBuilder result = new StringBuilder();
		String temp = null;
		for (int i = 0; i<10 ; i++) {
		    temp = (value && GamesUtil.getInstance(main).isChkSelected(i+1)) || (value && txt.equals(""+(i+1))) ? ""+(i+1):"";
			result.append("|"+temp);
		}
        Log.d(TAG, result.toString());
		GamesUtil.getInstance(main).setChkSelected(result.toString());
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.d(TAG, "onActivityCreated");
		super.onActivityCreated(savedInstanceState);
		SelectNumbersToPlayFragment sntpf = new SelectNumbersToPlayFragment();
		FragmentTransaction transaction = this.getFragmentManager().beginTransaction();
		transaction.replace(R.id.numbersToPlayFragment,sntpf,"numbers_2_play");
		transaction.commit();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Log.d(TAG, "onSaveInstanceState");
	}

	@Override
	public void onDetach() {
		Log.d(TAG, "onDetach");
		super.onDetach();
	}
}