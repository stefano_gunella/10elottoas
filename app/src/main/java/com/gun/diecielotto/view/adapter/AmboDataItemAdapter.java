package com.gun.diecielotto.view.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gun.diecielotto.R;
import com.gun.diecielotto.engine.data.AmboDataItem;
import com.gun.diecielotto.util.DateUtils;

public class AmboDataItemAdapter extends ArrayAdapter<AmboDataItem> {
	private final Context context;
	private final List<AmboDataItem> lvalues;

	public AmboDataItemAdapter(Context context, List<AmboDataItem> lvalues) {
		super(context, R.layout.list_ambi_layout, lvalues);
		this.context = context;
		this.lvalues = lvalues;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
//		int textColor = Color.WHITE;
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.list_ambi_layout, parent, false);
		TextView indexView = (TextView) rowView.findViewById(R.id.index);
		TextView imageView1 = (TextView) rowView.findViewById(R.id.icon1);
		TextView imageView2 = (TextView) rowView.findViewById(R.id.icon2);
		TextView labelView = (TextView) rowView.findViewById(R.id.n_ext);
		// TextView maxEstrattiView = (TextView) rowView.findViewById(R.id.maxEstrattiConsecutivi);
		TextView dtEstrazioneView = (TextView) rowView.findViewById(R.id.dtEstrazione);
		AmboDataItem item = lvalues.get(position);
//		if(item.maxEstrazioniConsecutive > 0){
//			rowView.setBackgroundColor(Color.GREEN);
//			textColor = Color.BLACK;
//		}
		if(item.isUltimaEstrazione){
			rowView.setBackgroundColor(Color.BLACK);
//			textColor = Color.WHITE;
		}
		indexView.setText(""+(position+1));
		imageView1.setText(""+item.ext_1);
		imageView2.setText(""+item.ext_2);
		labelView.setText(""+item.tot);
		// maxEstrattiView.setTextColor(textColor);
		// maxEstrattiView.setText(""+item.maxEstrazioniConsecutive);
//		dtEstrazioneView.setTextColor(textColor);
		dtEstrazioneView.setText(DateUtils.dateToStringFormatUI(item.dtEstrazione));
		return rowView;
	}
	
	@Override
	public AmboDataItem getItem(int position) {
		return lvalues.get(position);
	}
} 