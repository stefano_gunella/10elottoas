package com.gun.diecielotto.view;

import java.util.LinkedList;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gun.diecielotto.DieciELottoActivity;
import com.gun.diecielotto.R;
import com.gun.diecielotto.db.DbManager;
import com.gun.diecielotto.db.LottoDataItem;
import com.gun.diecielotto.engine.data.BaseItems;
import com.gun.diecielotto.engine.data.SingoloEstrattoDataItem;

public class ShowPredictionGridFragment extends Fragment{
	final String TAG = "ShowPredictionGrid";// max 23 char!!!
	DieciELottoActivity main = null;
	View rootView = null;
	TextView imageView = null;
	RelativeLayout layout = null;

	LinkedList<LottoDataItem> listEstrazioni = null;

	public long extRif1 = 0;
	public long extRif2 = 0;
	public long prevExtRif1 = 0;
	public long prevExtRif2 = 0;

	Boolean flgCalc = true; 

	SelectNumbersToPlayFragment sntpf = null;
	GridLayout gridNumeriEstrazioni = null;
	int[] estrazioni = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		Log.d(TAG, "onCreateView - ");
		rootView = inflater.inflate(R.layout.show_prediction_grid_fragment, container, false);
		layout = (RelativeLayout)rootView.findViewById(R.id.show_grid_layout);
		main = (DieciELottoActivity)this.getActivity();
		this.extRif1 = getArguments().getLong("extRif1");
		this.extRif2 = getArguments().getLong("extRif2");
		this.prevExtRif1 = extRif1;
		this.prevExtRif2 = extRif2;
		this.flgCalc = main.flgCalc;
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.d(TAG, "onActivityCreated - ");
		gridNumeriEstrazioni = (GridLayout)layout.findViewById(R.id.grid_number_buttons);
		_updateGridExtractions();
		sntpf = new SelectNumbersToPlayFragment();
		FragmentTransaction transaction = this.getFragmentManager().beginTransaction();
		transaction.replace(R.id.numbersToPlayFragment,sntpf,"numbers_2_play");
		transaction.commit();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Log.d(TAG, "onSaveInstanceState - ");
	}

	OnClickListener listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			String tag = (String) v.getTag();
			Log.d(TAG, "TAG:"+tag);
            main.listaDaGiocare.add(Integer.parseInt(tag));
			sntpf.updateButtonsListaDaGiocare();
		}
	};

	InserisciEstrazioniTask inserisciEstrazioniTask = null;
	private void _updateGridExtractions() {
		inserisciEstrazioniTask = new InserisciEstrazioniTask();
		inserisciEstrazioniTask.execute(extRif1,extRif2);
	}

	@Override
	public void onDetach() {
		Log.d(TAG, "onDetach - ");
		inserisciEstrazioniTask.cancel(true);
		super.onDetach();
	}

	public synchronized void updateStartExtOnGrid(int index) {
		LottoDataItem estrazione = null;
		if(index == 1 && extRif1 == extRif2-1){
			return;
		}
		if(index == -1){
			extRif1 --;
			estrazione = DbManager.getInstance(main).getExtractionById(extRif1);
		}
		if(index == 1 && listEstrazioni.size() > 0){
			estrazione = listEstrazioni.getFirst();
			extRif1 ++;
		}
		if(null!=estrazione){
			for (final Integer ext : estrazione.numeriEstratti) {
				if(index == -1){
					estrazioni[ext-1]++;
				}
				if(index == 1){
					estrazioni[ext-1]--;
				}
				TextView tv = (TextView)gridNumeriEstrazioni.getChildAt(ext-1);
				if(estrazioni[ext-1]==0){
					tv.setBackgroundResource(R.drawable.red_ball);
					tv.setText(""+ext);
				}
				else if(estrazioni[ext-1]==1){
					tv.setBackgroundResource(R.drawable.yellow_ball);
					tv.setText(""+ext);
				}
				else{
					tv.setBackgroundResource(R.drawable.blue_ball);
					tv.setText(""+estrazioni[ext-1]);
				}
			}
			if(index == -1){
				listEstrazioni.addFirst(estrazione);
			}
			if(index == 1){
				listEstrazioni.removeFirst();
			}
		}
		_updateLastExtGreen();
		prevExtRif1 = extRif1;
		gridNumeriEstrazioni.refreshDrawableState();
		updateIndex();
	}

	public synchronized void updateEndExtOnGrid(int index) {
		LottoDataItem estrazione = null;
		if(index == -1 && extRif2 == extRif1+1){
			return;
		}
		if(index == -1){
			estrazione = listEstrazioni.getLast();
			extRif2 --;
		}
		if(index == 1){
			extRif2 ++;
			estrazione = DbManager.getInstance(main).getExtractionById(extRif2);
			if (estrazione == null){
				extRif2 = prevExtRif2;
				return;
			}
		}
		for (final Integer ext : estrazione.numeriEstratti) {
			if(index == -1){
				estrazioni[ext-1]--;
			}
			if(index == 1){
				estrazioni[ext-1]++;
			}
			TextView tv = (TextView)gridNumeriEstrazioni.getChildAt(ext-1);
			if(estrazioni[ext-1]==0){
				tv.setBackgroundResource(R.drawable.red_ball);
				tv.setText(""+ext);
			}
			else if(estrazioni[ext-1]==1){
				tv.setBackgroundResource(R.drawable.yellow_ball);
				tv.setText(""+ext);
			}
			else{
				tv.setBackgroundResource(R.drawable.blue_ball);
				tv.setText(""+estrazioni[ext-1]);			
			}
		}
		if(index == -1){
			listEstrazioni.removeLast();
		}
		if(index == 1){
			_removeLastExtGreen();
			listEstrazioni.addLast(estrazione);
		}
		_updateLastExtGreen();
		prevExtRif2 = extRif2;
		gridNumeriEstrazioni.refreshDrawableState();
		updateIndex();
	}

	private void _removeLastExtGreen(){
		LottoDataItem estrazione = listEstrazioni.getLast();
		for (final Integer ext : estrazione.numeriEstratti) {
			TextView tv = (TextView)gridNumeriEstrazioni.getChildAt(ext-1);
			if(estrazioni[ext-1]==0){
				tv.setBackgroundResource(R.drawable.red_ball);
				tv.setText(""+ext);
			}
			else if(estrazioni[ext-1]==1){
				tv.setBackgroundResource(R.drawable.yellow_ball);
				tv.setText(""+ext);
			}
			else{
				tv.setBackgroundResource(R.drawable.blue_ball);
				tv.setText(""+estrazioni[ext-1]);			
			}
		}
	}

	private void _updateLastExtGreen(){
		LottoDataItem estrazione = listEstrazioni.getLast();
		for (final Integer ext : estrazione.numeriEstratti) {
			TextView tv = (TextView)gridNumeriEstrazioni.getChildAt(ext-1);
			tv.setBackgroundResource(R.drawable.green_ball);
//			tv.setText(""+ext);
		}
	}
	
	private void updateIndex(){
		TextView startExt = (TextView)rootView.findViewById(R.id.ID_START_EXT);
		startExt.setText(""+prevExtRif1);
		TextView endExt = (TextView)rootView.findViewById(R.id.ID_END_EXT);
		endExt.setText(""+prevExtRif2);
		TextView tv = (TextView)main.findViewById(R.id.messaggio_estrazioni);
		tv.setText("estrazioni:"+(listEstrazioni.size()));
	}

	public void updateGridByExt(LottoDataItem estrazione){
		TextView tv = null;
		for (final Integer ext : estrazione.numeriEstratti) {
			estrazioni[ext-1]++;
			tv = (TextView)gridNumeriEstrazioni.getChildAt(ext-1);
			if(estrazioni[ext-1]==0){
				tv.setBackgroundResource(R.drawable.red_ball);
				tv.setText(""+ext);
			}
			else if(estrazioni[ext-1]==1){
				tv.setBackgroundResource(R.drawable.yellow_ball);
				tv.setText(""+ext);
			}
			else{
				tv.setBackgroundResource(R.drawable.blue_ball);
				tv.setText(""+estrazioni[ext-1]);			
			}
		}
		gridNumeriEstrazioni.refreshDrawableState();
	}

	MyListener pressButtonListener = new MyListener();
	public class InserisciEstrazioniTask extends AsyncTask<Long, String, Long>{

		@Override
		protected Long doInBackground(Long... value) {
			Long extRif1 = value[0];
			Long extRif2 = value[1];
			estrazioni = new int[90];
			this.publishProgress("Sto calcolando le statistiche...");

			listEstrazioni = new LinkedList<LottoDataItem>(DbManager.getInstance(main).getListExtractionsFromEx1Ex2(extRif1, extRif2));
			main.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					_prepareNumbersButtons();
					int index = 0;
					for (final LottoDataItem estrazione : listEstrazioni) {
						index ++;
						updateGridByExt(estrazione);
					}
					InserisciEstrazioniTask.this.publishProgress("estrazioni:"+index);
				}
				
				private void _prepareNumbersButtons() {
					for (int n=0;n<90;n++) {
						TextView tv = new TextView(ShowPredictionGridFragment.this.getActivity());
						tv.setOnClickListener(listener);
						LayoutParams tlp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
						tv.setGravity(Gravity.CENTER);
						tv.setLayoutParams(tlp);
						tv.setBackgroundResource(R.drawable.red_ball);
						tv.setTextColor(getResources().getColor(android.R.color.white));
						tv.setText(""+(n+1));
						tv.setTag(""+(n+1));
						gridNumeriEstrazioni.addView(tv);
					}
				}
			});
			return (long)listEstrazioni.size();
		}

		protected void onProgressUpdate(String... message) {
			TextView tv = (TextView)main.findViewById(R.id.messaggio_estrazioni);
			tv.setText(message[0]);
		}

		protected void onPostExecute(Long cnt) {
			Log.d(TAG, "onPostExecute:"+cnt);
			updateIndex();
		}
	}

	class MyListener implements OnItemClickListener{
		@Override
		public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
			//			AmboDataItem item = adapter.getItem(position);
			Log.d(TAG, "onItemClick:"+adapter.getAdapter().getItem(position));
			BaseItems selectedItems = (BaseItems)adapter.getAdapter().getItem(position);
			SingoloEstrattoDataItem[] allExt = selectedItems.getAllExt();
			for (SingoloEstrattoDataItem singoloEstrattoDataItem : allExt) {		
				//				Spec rowSpan = GridLayout.spec(GridLayout.UNDEFINED, 1);
				//		        Spec colspan = GridLayout.spec(GridLayout.UNDEFINED, 1);
				//		        GridLayout.LayoutParams gridParam = new GridLayout.LayoutParams(rowSpan, colspan);
                main.listaDaGiocare.add(singoloEstrattoDataItem.ext);
			}
			sntpf.updateButtonsListaDaGiocare();
		}
	}
}