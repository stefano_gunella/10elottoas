package com.gun.diecielotto.view;

import java.util.List;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gun.diecielotto.DieciELottoActivity;
import com.gun.diecielotto.R;
import com.gun.diecielotto.engine.DataPrediction;
import com.gun.diecielotto.engine.data.AmboDataItem;
import com.gun.diecielotto.engine.data.BaseItems;
import com.gun.diecielotto.engine.data.SingoloEstrattoDataItem;
import com.gun.diecielotto.util.GamesUtil;
import com.gun.diecielotto.view.adapter.AmboDataItemAdapter;
import com.gun.diecielotto.view.adapter.SingoloEstrattoDataItemAdapter;

public class ShowPredictionFragment extends Fragment{
	final String TAG = "ShowPredictionFragment";
	DieciELottoActivity main = null;
	View rootView = null;

	TextView imageView = null;
	FrameLayout layout = null;
	ListView listViewSingoli = null;
	ListView listViewAmbi = null;
//	ListView listViewTerni = null;

	List<SingoloEstrattoDataItem> listaSingoli = null;
	List<AmboDataItem> listaAmbi = null;
	SelectNumbersToPlayFragment sntpf = null;
	
	public static ShowPredictionFragment getInstance() {
		ShowPredictionFragment fragment = new ShowPredictionFragment();
        return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		Log.d(TAG, "onCreateView - ");
		rootView = inflater.inflate(R.layout.show_prediction_fragment, container, false);
		layout = (FrameLayout)rootView.findViewById(R.id.show_result_layout);
		listViewSingoli = (ListView)rootView.findViewById(R.id.list_view_singoli);
		listViewAmbi 	= (ListView)rootView.findViewById(R.id.list_view_ambi);
		main = (DieciELottoActivity)this.getActivity();
		if(main.flgCalc || listaAmbi == null || listaSingoli ==  null){
			_calcolaCombinazioni();
		}
		else{
			AmboDataItemAdapter adapterAmbi = new AmboDataItemAdapter(ShowPredictionFragment.this.getActivity(), listaAmbi);
			SingoloEstrattoDataItemAdapter adapterSingoli = new SingoloEstrattoDataItemAdapter(ShowPredictionFragment.this.getActivity(), listaSingoli);
			listViewAmbi.setAdapter(adapterAmbi);
			listViewSingoli.setAdapter(adapterSingoli);
			listViewAmbi.setOnItemClickListener(pressButtonListener);
			listViewSingoli.setOnItemClickListener(pressButtonListener);
			ProgressBar pb = (ProgressBar)rootView.findViewById(R.id.progress);
			((ViewGroup)pb.getParent()).removeView(rootView.findViewById(R.id.progress));
			TextView tv = (TextView)main.findViewById(R.id.messaggio_estrazioni);
			tv.setText("Totale estrazioni:"+dp.getTotaleEstrazioni());
		}
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.d(TAG, "onActivityCreated - ");
		super.onActivityCreated(savedInstanceState);
		sntpf = new SelectNumbersToPlayFragment();
		FragmentTransaction transaction = this.getFragmentManager().beginTransaction();
		transaction.replace(R.id.numbersToPlayFragment,sntpf,"numbers_2_play");
		transaction.commit();
	}

	public void buttonSingoliShowListTopDown() {
		int fvp = listViewSingoli.getFirstVisiblePosition();
		if(fvp > listaSingoli.size()/2){
			listViewSingoli.setSelection(0);
		}
		else{
			listViewSingoli.setSelection(listaAmbi.size());
		}
	}

	public void buttonAmbiShowListTopDown() {
		int fvp = listViewAmbi.getFirstVisiblePosition();
		if(fvp > listaAmbi.size()/2){
			listViewAmbi.setSelection(0);
		}
		else{
			listViewAmbi.setSelection(listaAmbi.size());
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Log.d(TAG, "onSaveInstanceState - ");
	}
	
	CalcolaCombinazioniTask calcolaCombinazioniTask = null;
	private void _calcolaCombinazioni() {
		calcolaCombinazioniTask = new CalcolaCombinazioniTask();
		calcolaCombinazioniTask.execute(GamesUtil.getInstance(main).getExtStat(),GamesUtil.getInstance(main).getExtDelta());
	}

	@Override
	public void onDetach() {
		Log.d(TAG, "onDetach - ");
		calcolaCombinazioniTask.stopCalcolo();
		calcolaCombinazioniTask.cancel(true);
		super.onDetach();
	}

	MyListener pressButtonListener = new MyListener();
	DataPrediction dp = null;
	public class CalcolaCombinazioniTask extends AsyncTask<Integer, String, Integer>{

		@Override
		protected Integer doInBackground(Integer... value) {
			Integer extStat = value[0];
			Integer extDelta = value[1];
			dp = new DataPrediction(ShowPredictionFragment.this.getActivity());
			this.publishProgress("Sto calcolando le statistiche...");// richiama onProgressUpdate...
			dp.calcolaStatistiche(extStat,extDelta);
			this.publishProgress("Totale estrazioni:"+dp.getTotaleEstrazioni());
			listaAmbi = dp.getAmbiRicorrenti();
			listaSingoli = dp.getEstrattiRicorrenti();
			return listaAmbi.size();
		}

		public void stopCalcolo() {
			dp.stopPredicton = true;
		}

		protected void onProgressUpdate(String... message) {
			Log.d(TAG, "onProgressUpdate:");
			TextView tv = (TextView)main.findViewById(R.id.messaggio_estrazioni);
			tv.setText(message[0]);
		}

		protected void onPostExecute(Integer result) {
			Log.d(TAG, "onPostExecute:"+result);
			AmboDataItemAdapter adapterAmbi = new AmboDataItemAdapter(ShowPredictionFragment.this.getActivity(), listaAmbi);
			SingoloEstrattoDataItemAdapter adapterSingoli = new SingoloEstrattoDataItemAdapter(ShowPredictionFragment.this.getActivity(), listaSingoli);
			listViewAmbi.setAdapter(adapterAmbi);
			listViewSingoli.setAdapter(adapterSingoli);
			listViewAmbi.setOnItemClickListener(pressButtonListener);
			listViewSingoli.setOnItemClickListener(pressButtonListener);
			ProgressBar pb = (ProgressBar)rootView.findViewById(R.id.progress);
			((ViewGroup)pb.getParent()).removeView(rootView.findViewById(R.id.progress));
		}
	}

	class MyListener implements OnItemClickListener{
		@Override
		public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
			//			AmboDataItem item = adapter.getItem(position);
			Log.d(TAG, "onItemClick:"+adapter.getAdapter().getItem(position));
			BaseItems selectedItems = (BaseItems)adapter.getAdapter().getItem(position);
			SingoloEstrattoDataItem[] allExt = selectedItems.getAllExt();
			for (SingoloEstrattoDataItem singoloEstrattoDataItem : allExt) {		
				//				Spec rowSpan = GridLayout.spec(GridLayout.UNDEFINED, 1);
				//		        Spec colspan = GridLayout.spec(GridLayout.UNDEFINED, 1);
				//		        GridLayout.LayoutParams gridParam = new GridLayout.LayoutParams(rowSpan, colspan);
                main.listaDaGiocare.add(singoloEstrattoDataItem.ext);
			}
			sntpf.updateButtonsListaDaGiocare();
		}
	}
}