package com.gun.diecielotto.db;

import java.util.ArrayList;
import java.util.Arrays;


/****
 * {"data":20160102,
 * "progressivoGiornaliero":11,
 * "numeriGiocati":["4","6","8","19","22","35","39","43","48","56","62","66","68","69","75","76","77","83","88","89"],
 * "numeroSpeciale":8}
 * @author stefano
 */
public class GiocateLottoDataItem extends BaseDataItem{
	private static final String header = "_id,data,progressivoGiornaliero,e1,e2,e3,e4,e5,e6,e7,e8,e9,e10,numeroSpeciale,ext_stat,ext_delta,numeroGiocateDifferenza\n"; // serve x export - appena riesco a farlo
	public boolean flg_h = false;
	public long id_ext;			// indice dell'estrazione...
	public int[] numeriGiocati = new int[10];
	public int numeroSpeciale;
	public long extStat;
	public int vincita;			// vincita ultima
	public int vincitaTot;		// vincita totale
	public int n_giocate_valide;
	public long ext_diff_max_win;
	
	public int getTotaleNumeriGiocati(){
		int result = 0;
		for (int numero : numeriGiocati) {
			result = (numero > 0) ? result+1 : result;
		}
		return result;
	}
	
	public ArrayList<String> getNumeriGiocati(){
		Arrays.sort(numeriGiocati);
		ArrayList<String> lresult = new ArrayList<String>();
		for (int numero : numeriGiocati) {
			if(numero!=0){
				lresult.add(""+numero);
			}
		}
		return lresult;
	}
	
	public long getIndexExt(){
		return id_ext;
	}

	@Override
	public String toString() {
		String result = null;
		if(flg_h){
			return header;
		}
		StringBuilder sb = new StringBuilder();
		for (int e : numeriGiocati) {
			if(e>0){
				sb.append(","+e);
			}
		}
		result = sb.toString().length() > 0 ? "ID:"+_id+",id_ext:"+id_ext+",#NGV:"+n_giocate_valide+","+data+",PG:"+progressivoGiornaliero+",Ex:"+sb.toString().substring(1)+",NS:"+numeroSpeciale+",EXT Stat:"+extStat+"EXT Delta:"+ext_delta+" attesa:"+ext_diff+"\n":null;
		return result;
	}
}
