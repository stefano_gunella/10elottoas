package com.gun.diecielotto.db;

import android.provider.BaseColumns;

/****
 * {"data":yyyyMMdd,
 * "progressivoGiornaliero":11,
 * "massimoProgressivoGiornaliero":null,
 * "numeriEstratti":["4","6","8","19","22","35","39","43","48","56","62","66","68","69","75","76","77","83","88","89"],
 * "numeroSpeciale":8}
 * @author stefano
 */
public abstract class LottoDataStruct implements BaseColumns {
	public static final String TABLE_NAME = "_10elotto"; //non pu? cominciare per un numero.
	public static final String DATA = "data";
	public static final String PG = "progressivoGiornaliero";
	public static final String MPG = "massimoProgressivoGiornaliero";
	public static final String _1 = "e1";
	public static final String _2 = "e2";
	public static final String _3 = "e3";
	public static final String _4 = "e4";
	public static final String _5 = "e5";
	public static final String _6 = "e6";
	public static final String _7 = "e7";
	public static final String _8 = "e8";
	public static final String _9 = "e9";
	public static final String _10 = "e10";
	public static final String _11 = "e11";
	public static final String _12 = "e12";
	public static final String _13 = "e13";
	public static final String _14 = "e14";
	public static final String _15 = "e15";
	public static final String _16 = "e16";
	public static final String _17 = "e17";
	public static final String _18 = "e18";
	public static final String _19 = "e19";
	public static final String _20 = "e20";
	public static final String NS = "numeroSpeciale";
}
