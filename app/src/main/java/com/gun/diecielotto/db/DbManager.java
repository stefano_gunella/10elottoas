package com.gun.diecielotto.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Environment;
import android.util.Log;

import com.gun.diecielotto.util.DateUtils;
import com.gun.diecielotto.util.file.FileManager;

public class DbManager{
	private static DbManager dbManager = null;
	private static final String TAG = "DbManager";
	private LottoDbHelper dbhelper = null;
	Context ctx = null;

	String[] columnsEstrazioni = {
			LottoDataStruct._ID,
			LottoDataStruct.DATA,
			LottoDataStruct.PG,
			LottoDataStruct.MPG,
			LottoDataStruct._1,
			LottoDataStruct._2,
			LottoDataStruct._3,
			LottoDataStruct._4,
			LottoDataStruct._5,
			LottoDataStruct._6,
			LottoDataStruct._7,
			LottoDataStruct._8,
			LottoDataStruct._9,
			LottoDataStruct._10,
			LottoDataStruct._11,
			LottoDataStruct._12,
			LottoDataStruct._13,
			LottoDataStruct._14,
			LottoDataStruct._15,
			LottoDataStruct._16,
			LottoDataStruct._17,
			LottoDataStruct._18,
			LottoDataStruct._19,
			LottoDataStruct._20,
			LottoDataStruct.NS
	};
	
	private DbManager(Context ctx){
		dbhelper=new LottoDbHelper(ctx);
		this.ctx = ctx;
	}

	public static DbManager getInstance(Context ctx){
		if(null == dbManager){
			dbManager = new DbManager(ctx);
		}
		return dbManager;
	}

	public synchronized boolean drop(){
		return ctx.deleteDatabase(LottoDbHelper.DATABASE_NAME);
	}

	public synchronized void close(){
		dbhelper.close();
	}

	public synchronized void saveEstrazione(LottoDataItem item){
		SQLiteDatabase db = dbhelper.getWritableDatabase();
		ContentValues cv=new ContentValues();
		cv.put(LottoDataStruct._ID, item._id);
		cv.put(LottoDataStruct.DATA, item.data);
		cv.put(LottoDataStruct.PG, item.progressivoGiornaliero);
		cv.put(LottoDataStruct.MPG, item.massimoProgressivoGiornaliero);
		cv.put(LottoDataStruct._1, item.numeriEstratti[0]);
		cv.put(LottoDataStruct._2, item.numeriEstratti[1]);
		cv.put(LottoDataStruct._3, item.numeriEstratti[2]);
		cv.put(LottoDataStruct._4, item.numeriEstratti[3]);
		cv.put(LottoDataStruct._5, item.numeriEstratti[4]);
		cv.put(LottoDataStruct._6, item.numeriEstratti[5]);
		cv.put(LottoDataStruct._7, item.numeriEstratti[6]);
		cv.put(LottoDataStruct._8, item.numeriEstratti[7]);
		cv.put(LottoDataStruct._9, item.numeriEstratti[8]);
		cv.put(LottoDataStruct._10, item.numeriEstratti[9]);
		cv.put(LottoDataStruct._11, item.numeriEstratti[10]);
		cv.put(LottoDataStruct._12, item.numeriEstratti[11]);
		cv.put(LottoDataStruct._13, item.numeriEstratti[12]);
		cv.put(LottoDataStruct._14, item.numeriEstratti[13]);
		cv.put(LottoDataStruct._15, item.numeriEstratti[14]);
		cv.put(LottoDataStruct._16, item.numeriEstratti[15]);
		cv.put(LottoDataStruct._17, item.numeriEstratti[16]);
		cv.put(LottoDataStruct._18, item.numeriEstratti[17]);
		cv.put(LottoDataStruct._19, item.numeriEstratti[18]);
		cv.put(LottoDataStruct._20, item.numeriEstratti[19]);
		cv.put(LottoDataStruct.NS, item.numeroSpeciale);
		try{
			db.insert(LottoDataStruct.TABLE_NAME, null,cv);
		}
		catch (SQLiteException sqle){
			Log.d(TAG,"Errore:"+sqle.getMessage());
		}
        finally {
			Log.d(TAG,"DB_CLOSE:");
            db.close();
        }
	}

	/***
	 * Recupera una certa estrazione ad una certa data
	 * lasciare SYNCHRONIZED altrimenti si rompe tutto!!!
	 * @param date
	 * @param extraction
	 * @return
	 */
	public synchronized LottoDataItem find(String date, int extraction){
		SQLiteDatabase db=dbhelper.getReadableDatabase();
		LottoDataItem result = null;
		try{
			Cursor c = db.rawQuery(	
					"SELECT * FROM "+LottoDataStruct.TABLE_NAME+
					" WHERE "+LottoDataStruct.DATA+"=? AND "+LottoDataStruct.PG+"=?",new String[]{date,String.valueOf(extraction)});
			if(c.moveToFirst()){
				result = _cursor2LottoDataItem(c);
			}
		}
		catch (SQLiteException sqle){
			Log.d(TAG,"Errore:"+sqle);
		}
        finally {
            db.close();
        }
		return result;
	}

	/***
	 * Tutte le estrazioni a partire da una data
	 * @param date
	 * @return
	 */
	public synchronized List<LottoDataItem> loadAllExtractionsFromDate(String date, Integer extDelta){
		SQLiteDatabase db=dbhelper.getReadableDatabase();
		List<LottoDataItem> lresult = new ArrayList<LottoDataItem>();
		try{
			Cursor c = db.rawQuery(	
					"SELECT * FROM "+LottoDataStruct.TABLE_NAME+
					" WHERE "+LottoDataStruct.DATA+">=? AND "+LottoDataStruct.PG+"!=0 ORDER BY "+LottoDataStruct.DATA +" ASC, "+LottoDataStruct.PG +" ASC OFFSET ?",new String[]{date,"extDelta"});
//					" WHERE "+LottoDataStruct.DATA+">=? AND "+LottoDataStruct.NS+" is NOT NULL ORDER BY "+LottoDataStruct.DATA +" ASC",new String[]{date});
			while(c.moveToNext()){
				lresult.add(_cursor2LottoDataItem(c));
			}
		}
		catch (SQLiteException sqle){
			Log.d(TAG,"Errore:"+sqle);
		}
        finally {
			Log.d(TAG,"DB_CLOSE:");
            db.close();
        }
		return lresult;
	}

	/***
	 * Tutte le ultime N estrazioni
     * @param extStat
     * @param extDelta
	 * @return
	 */
	public synchronized List<LottoDataItem> loadLastNExtractions(Integer extStat, Integer extDelta){
		SQLiteDatabase db=dbhelper.getReadableDatabase();
		long extIndex	   = DateUtils.getIndexNow();		// Ultima estrazione del giorno...
		long extIndexStart = extIndex - extDelta - extStat;
		long extIndexStop  = extIndex - extDelta;
		List<LottoDataItem> lresult = new ArrayList<LottoDataItem>();
		try{
			Cursor c = db.rawQuery(	
					"SELECT * FROM "+LottoDataStruct.TABLE_NAME+
					" WHERE "+LottoDataStruct._ID +">? AND "+LottoDataStruct._ID+"<=?",new String[]{""+extIndexStart,""+extIndexStop});
			while(c.moveToNext()){
				lresult.add(_cursor2LottoDataItem(c));
			}
		}
		catch (SQLiteException sqle){
			Log.d(TAG,"Errore:"+sqle);
		}
        finally {
			Log.d(TAG,"DB_CLOSE:");
            db.close();
        }
		return lresult;
	}

	private LottoDataItem _cursor2LottoDataItem(Cursor c) {
		LottoDataItem item = new LottoDataItem();
		item._id = c.getLong(c.getColumnIndex(LottoDataStruct._ID));
		item.data = c.getString(c.getColumnIndex(LottoDataStruct.DATA));
		item.progressivoGiornaliero = c.getInt(c.getColumnIndex(LottoDataStruct.PG));
		item.massimoProgressivoGiornaliero = c.getInt(c.getColumnIndex(LottoDataStruct.MPG));
		item.numeriEstratti[0] = c.getInt(c.getColumnIndex(LottoDataStruct._1));
		item.numeriEstratti[1] = c.getInt(c.getColumnIndex(LottoDataStruct._2));
		item.numeriEstratti[2] = c.getInt(c.getColumnIndex(LottoDataStruct._3));
		item.numeriEstratti[3] = c.getInt(c.getColumnIndex(LottoDataStruct._4));
		item.numeriEstratti[4] = c.getInt(c.getColumnIndex(LottoDataStruct._5));
		item.numeriEstratti[5] = c.getInt(c.getColumnIndex(LottoDataStruct._6));
		item.numeriEstratti[6] = c.getInt(c.getColumnIndex(LottoDataStruct._7));
		item.numeriEstratti[7] = c.getInt(c.getColumnIndex(LottoDataStruct._8));
		item.numeriEstratti[8] = c.getInt(c.getColumnIndex(LottoDataStruct._9));
		item.numeriEstratti[9] = c.getInt(c.getColumnIndex(LottoDataStruct._10));
		item.numeriEstratti[10] = c.getInt(c.getColumnIndex(LottoDataStruct._11));
		item.numeriEstratti[11] = c.getInt(c.getColumnIndex(LottoDataStruct._12));
		item.numeriEstratti[12] = c.getInt(c.getColumnIndex(LottoDataStruct._13));
		item.numeriEstratti[13] = c.getInt(c.getColumnIndex(LottoDataStruct._14));
		item.numeriEstratti[14] = c.getInt(c.getColumnIndex(LottoDataStruct._15));
		item.numeriEstratti[15] = c.getInt(c.getColumnIndex(LottoDataStruct._16));
		item.numeriEstratti[16] = c.getInt(c.getColumnIndex(LottoDataStruct._17));
		item.numeriEstratti[17] = c.getInt(c.getColumnIndex(LottoDataStruct._18));
		item.numeriEstratti[18] = c.getInt(c.getColumnIndex(LottoDataStruct._19));
		item.numeriEstratti[19] = c.getInt(c.getColumnIndex(LottoDataStruct._20));
		item.numeroSpeciale = c.getInt(c.getColumnIndex(LottoDataStruct.NS));
		return item;
	}

	public synchronized boolean deleteEstrazione(long id){
		SQLiteDatabase db=dbhelper.getWritableDatabase();
		try{
            return db.delete(LottoDataStruct.TABLE_NAME, LottoDataStruct._ID + "=?", new String[]{Long.toString(id)}) > 0;
        }
		catch (SQLiteException sqle){
			return false;
		}
        finally {
			Log.d(TAG,"DB_CLOSE:");
            db.close();
        }
    }
	
	/***
	 * lasciare SYNCHRONIZED altrimenti si rompe tutto!!!
	 * @param id
	 * @return
	 */
	public synchronized LottoDataItem getExtractionById(long id){
		Cursor cursor=null;
		String whereClause = LottoDataStruct._ID + "=?";
		String[] whereArgs = {String.valueOf(id)};
		LottoDataItem result = null;
        SQLiteDatabase db = dbhelper.getReadableDatabase();
		try{
		    cursor = db.query(LottoDataStruct.TABLE_NAME,
			columnsEstrazioni,	// The columns to return
			whereClause,		// The columns for the WHERE clause
			whereArgs,			// The values for the WHERE clause
			null,				// don't group the rows
			null,				// don't filter by row groups
			null				// don't sort
			);
			if(cursor.moveToFirst()){
				result = _cursor2LottoDataItem(cursor);
			}
		}
		catch(IllegalStateException ise){
			Log.d(TAG,"Exception:"+ise.getMessage());
			return null;
		}
		catch(SQLiteException sqle){
			Log.d(TAG,"Exception:"+sqle.getMessage());
			return null;
		}
        finally {
			Log.d(TAG,"DB_CLOSE:");
            db.close();
        }
		return result;
	}

	/*
	public synchronized LottoDataItem getExtractionAtDate(Date date, int nExtraction){
		Cursor cursor = null;
		String dateStr = DateUtils.dateToStringFormat(date);
		String selection = LottoDataStruct.DATA + "=? AND "+LottoDataStruct.PG +"=?";
		String[] selectionArgs = {dateStr,String.valueOf(nExtraction)};
		String sortOrder = LottoDataStruct.DATA + " DESC";
        SQLiteDatabase db = dbhelper.getReadableDatabase();
		try{
			cursor = db.query(LottoDataStruct.TABLE_NAME,
				columnsEstrazioni,	// The columns to return
				selection,			// The columns for the WHERE clause
				selectionArgs,		// The values for the WHERE clause
				null,		// don't group the rows
				null,		// don't filter by row groups
				sortOrder			// The sort order
				);
		}
		catch(SQLiteException sqle){
			return null;
		}
        finally {
			Log.d(TAG,"DB_CLOSE:");
            db.close();
        }
		return _cursor2LottoDataItem(cursor);
	}
	*/

	@Deprecated
	public synchronized int getIdLastExtractionAtDate(Date date){
		Cursor cursor=null;
		String[] selectionArgs = {DateUtils.dateToStringFormat(date)};
        SQLiteDatabase db = dbhelper.getReadableDatabase();
		try{
			cursor = db.rawQuery("SELECT MAX ("+LottoDataStruct.PG +") FROM "+LottoDataStruct.TABLE_NAME +
								 " WHERE "+LottoDataStruct.DATA + "=?", selectionArgs);
			cursor.moveToFirst();
		}
		catch(SQLiteException sqle){
			return -1;
		}
        finally {
			Log.d(TAG,"DB_CLOSE:");
            db.close();
        }
		return cursor.getInt(0);
	}

	/***
	 * Restituisce l'ultima estrazione di oggi.
	 * @return
	 */
	public synchronized LottoDataItem getLastExtractionNow(){
		LottoDataItem result = null;
		long idUltimaEstrazione = DateUtils.getIndexNow();
		result = this.getExtractionById(idUltimaEstrazione);
		return result;
	}
	
	/***
	 * DA RIVEDERE - non riesco ad esportarlo :(
	 * @param databaseName
	 */
	public synchronized void exportDatabase(String databaseName) {
		try {
			String destSD = FileManager.getInstance().absPath;
			File data = Environment.getDataDirectory();

			if (Environment.getExternalStorageDirectory().canWrite()) {
				String currentDBPath = "//data//"+ctx.getPackageName()+"//databases//"+databaseName;
				String backupDBPath = LottoDbHelper.DATABASE_NAME;
				File currentDB = new File(data, currentDBPath);
				File backupDB = new File(destSD, backupDBPath);

				if (currentDB.exists()) {
					FileChannel src = new FileInputStream(currentDB).getChannel();
					FileChannel dst = new FileOutputStream(backupDB).getChannel();
					dst.transferFrom(src, 0, src.size());
					src.close();
					dst.close();
				}
			}
		}
		catch (Exception e) {

		}
	}

	public synchronized void importDatabase(String databaseName) {
		try {
			InputStream assetDB = ctx.getApplicationContext().getAssets().open(databaseName); 
//			String destDB = Environment.getDataDirectory().getAbsolutePath();

			if (Environment.getExternalStorageDirectory().canWrite()) {
				String currentDBPath = ctx.getApplicationInfo().dataDir+"/databases/"+databaseName;
//				String currentDBPath = destDB+"/"+ctx.getPackageName()+"/databases/"+databaseName;
				File currentDB = new File(currentDBPath);
				if (!currentDB.exists() && currentDB.canWrite()) {
				    OutputStream mOutput = new FileOutputStream(new File(currentDBPath)); 
				    byte[] mBuffer = new byte[1024]; 
				    int mLength; 
				    while ((mLength = assetDB.read(mBuffer))>0){ 
				        mOutput.write(mBuffer, 0, mLength); 
				    } 
				    mOutput.flush(); 
				    mOutput.close(); 
				    assetDB.close(); 
				}
			}
		}
		catch (Exception e) {
			Log.d(TAG, e.toString());
		}
	}

	public synchronized int getNumeroEstrazioniDallaData(Date date) {
		SQLiteDatabase db=dbhelper.getReadableDatabase();
		String queryString = "SELECT COUNT(*) FROM " + LottoDataStruct.TABLE_NAME +" WHERE "+LottoDataStruct.NS+" is NOT NULL";
		Cursor c = db.rawQuery(queryString, null);
		c.moveToFirst();
		int count = c.getInt(0);
		Log.d(TAG,"DB_CLOSE:");
        db.close();
		return count;
	}
	
	public synchronized ArrayList<LottoDataItem> getListExtractionsFrom(BaseDataItem item, boolean flgDelta){
		ArrayList<LottoDataItem> lresult = new ArrayList<LottoDataItem>();
		long delta = flgDelta ? item.ext_delta : 0;
        SQLiteDatabase db = dbhelper.getReadableDatabase();
		try{
			Cursor cursor = db.rawQuery(
				"SELECT * FROM "+LottoDataStruct.TABLE_NAME+
				" WHERE "+LottoDataStruct._ID +">? ORDER BY "+LottoDataStruct._ID + " ASC",new String[]{""+(item.getIndexExt()-delta)});
			while(cursor.moveToNext()){
				lresult.add(_cursor2LottoDataItem(cursor));
			}
		}
		catch(SQLiteException sqle){
			return null;
		}
        finally {
			Log.d(TAG,"DB_CLOSE:");
            db.close();
        }
		return lresult;
	}

	/***
	 * @param index1 prima estrazione - la piu' vecchia
	 * @param index2 seconda estrazione - la piu' recente
	 * @return
	 */
	public synchronized ArrayList<LottoDataItem> getListExtractionsFromEx1Ex2(long index1, long index2){
		ArrayList<LottoDataItem> lresult = new ArrayList<LottoDataItem>();
        SQLiteDatabase db = dbhelper.getReadableDatabase();
		try{
			Cursor cursor = db.rawQuery(
				"SELECT * FROM "+LottoDataStruct.TABLE_NAME+
				" WHERE "+LottoDataStruct._ID +">=? AND "+LottoDataStruct._ID +"<=? ORDER BY "+LottoDataStruct._ID + " ASC",new String[]{""+index1,""+index2});
			while(cursor.moveToNext()){
				lresult.add(_cursor2LottoDataItem(cursor));
			}
		}
		catch(SQLiteException sqle){
			return null;
		}
        finally {
			Log.d(TAG,"DB_CLOSE:");
            db.close();
        }
		return lresult;
	}

	/*
	================
	GESTIONE GIOCATE
	================
	*/
	
	/***
	 * Una giocata puo' contenere da 1 a 10 numeri.
	 * Tutte le giocate inserite vengono calcolate valide nel calcolo dell'importo della giocata.
	 * Non si possono inserire 2 giocate con gli stessi numeri.
	 * Non viene mantenuto lo storico.
	 * @param item
	 */
	public synchronized void saveGiocate(GiocateLottoDataItem item){
		List<GiocateLottoDataItem> lGames = getListGiocateBy(item); // vediamo se esiste gia' una giocta uguale
		if(lGames.size() > 0 ){
			Log.d(TAG,"saveGiocate - GIOCATA DOPPIA:"+item);
			return;
		}
		SQLiteDatabase db = dbhelper.getWritableDatabase();
		ContentValues cv=new ContentValues();
		cv.put(GiocateLottoDataStruct.DATA, item.data);
		cv.put(GiocateLottoDataStruct.ID_EXT, item.id_ext);
		cv.put(GiocateLottoDataStruct.PG, item.progressivoGiornaliero);
		cv.put(GiocateLottoDataStruct._1, item.numeriGiocati[0]);
		cv.put(GiocateLottoDataStruct._2, item.numeriGiocati[1]);
		cv.put(GiocateLottoDataStruct._3, item.numeriGiocati[2]);
		cv.put(GiocateLottoDataStruct._4, item.numeriGiocati[3]);
		cv.put(GiocateLottoDataStruct._5, item.numeriGiocati[4]);
		cv.put(GiocateLottoDataStruct._6, item.numeriGiocati[5]);
		cv.put(GiocateLottoDataStruct._7, item.numeriGiocati[6]);
		cv.put(GiocateLottoDataStruct._8, item.numeriGiocati[7]);
		cv.put(GiocateLottoDataStruct._9, item.numeriGiocati[8]);
		cv.put(GiocateLottoDataStruct._10,item.numeriGiocati[9]);
		cv.put(GiocateLottoDataStruct.NS, item.numeroSpeciale);
		cv.put(GiocateLottoDataStruct.EXT_STAT, item.extStat);
		cv.put(GiocateLottoDataStruct.EXT_DELTA, item.ext_delta);
		cv.put(GiocateLottoDataStruct.EXT_DIFF, item.ext_diff);
		cv.put(GiocateLottoDataStruct.WIN,item.vincita);
		cv.put(GiocateLottoDataStruct.WIN_TOT,item.vincitaTot);
		cv.put(GiocateLottoDataStruct.IS_MAX_WIN,item.is_max_vincita ? 1:0 );
		cv.put(GiocateLottoDataStruct.N_GIOCATE_VALIDE,item.n_giocate_valide);
		try{
			Log.d(TAG,"GIOCATA:"+item);
			db.insert(GiocateLottoDataStruct.TABLE_NAME, null,cv);
		}
		catch (SQLiteException sqle){
			Log.d(TAG,"saveGiocate Errore:"+sqle);
		}
        finally {
			Log.d(TAG,"DB_CLOSE:");
            db.close();
        }
	}

	/***
	 * Recupera tutte le giocate con gli stessi numeri di item.
	 * @param item
	 * @return
	 */
	public synchronized List<GiocateLottoDataItem> getListGiocateBy(GiocateLottoDataItem item){
		SQLiteDatabase db=dbhelper.getReadableDatabase();
		List<GiocateLottoDataItem> lresult = new ArrayList<GiocateLottoDataItem>();
		String[] sNumeriGiocati = new String[10];
		try{
			for (int i=0; i<10; i++) {
				sNumeriGiocati[i] = item.numeriGiocati[i] == 0 ? "0":""+item.numeriGiocati[i];
			}
			Cursor c = db.rawQuery(	
				"SELECT * FROM "+GiocateLottoDataStruct.TABLE_NAME+" WHERE "+
				GiocateLottoDataStruct._1+"=? AND "+
				GiocateLottoDataStruct._2+"=? AND "+
				GiocateLottoDataStruct._3+"=? AND "+
				GiocateLottoDataStruct._4+"=? AND "+
				GiocateLottoDataStruct._5+"=? AND "+
				GiocateLottoDataStruct._6+"=? AND "+
				GiocateLottoDataStruct._7+"=? AND "+
				GiocateLottoDataStruct._8+"=? AND "+
				GiocateLottoDataStruct._9+"=? AND "+
				GiocateLottoDataStruct._10+"=? ",
				sNumeriGiocati);
			while(c.moveToNext()){
				lresult.add(_cursor2GiocataLottoDataItem(c));
			}
		}
		catch (SQLiteException sqle){
			Log.d(TAG,"Errore:"+sqle);
		}
        finally {
			Log.d(TAG,"saveGiocate - DB_CLOSE:");
            db.close();
        }
		return lresult;
	}	

	public synchronized List<GiocateLottoDataItem> getListGiocate(){
		SQLiteDatabase db=dbhelper.getReadableDatabase();
		List<GiocateLottoDataItem> lresult = new ArrayList<GiocateLottoDataItem>();
		try{
			Cursor c = db.rawQuery(
				"SELECT * FROM "+GiocateLottoDataStruct.TABLE_NAME,
				null);
			while(c.moveToNext()){
				lresult.add(_cursor2GiocataLottoDataItem(c));
			}
		}
		catch (SQLiteException sqle){
			Log.d(TAG,"Errore:"+sqle);
		}
        finally {
			Log.d(TAG,"getListGiocate - DB_CLOSE:");
            db.close();
        }
		return lresult;
	}	
	
	public synchronized GiocateLottoDataItem getGiocataById(int id){
		SQLiteDatabase db=dbhelper.getReadableDatabase();
		GiocateLottoDataItem result = null;
		try{
			Cursor c = db.rawQuery(	
					"SELECT * FROM "+GiocateLottoDataStruct.TABLE_NAME+
					" WHERE "+GiocateLottoDataStruct._ID+"=?",new String[]{""+id});
			if(c.moveToFirst()){
				result = _cursor2GiocataLottoDataItem(c);
			}
		}
		catch (SQLiteException sqle){
			Log.d(TAG,"Errore:"+sqle);
		}
        finally {
			Log.d(TAG,"DB_CLOSE:");
            db.close();
        }
		return result;
	}

	public synchronized int updateVincita(GiocateLottoDataItem item){
		int result = 0;
		SQLiteDatabase db = dbhelper.getWritableDatabase();
		ContentValues values=new ContentValues();
		values.put(GiocateLottoDataStruct.WIN, item.vincita);
		values.put(GiocateLottoDataStruct.WIN_TOT, item.vincitaTot);
		values.put(GiocateLottoDataStruct.EXT_DIFF, item.ext_diff);
		if(item.is_max_vincita){
			values.put(GiocateLottoDataStruct.IS_MAX_WIN, item.is_max_vincita ? 1:0);
		}
		String whereClause = GiocateLottoDataStruct._ID +"=?";
		String[] whereArgs = {item._id+""};
		try{
			result = db.update(GiocateLottoDataStruct.TABLE_NAME, values, whereClause, whereArgs);
		}
		catch (SQLiteException sqle){
			Log.d(TAG,"saveGiocate Errore:"+sqle);
		}
        finally {
			Log.d(TAG,"DB_CLOSE:");
            db.close();
        }
        return result;
	}

	public synchronized void deleteAllGames(){
		SQLiteDatabase db=dbhelper.getWritableDatabase();
		db.delete(GiocateLottoDataStruct.TABLE_NAME, null, null);
	}

	public synchronized boolean deleteGame(long id){
		SQLiteDatabase db=dbhelper.getWritableDatabase();
		boolean result = false;
		try{
			int n = db.delete(GiocateLottoDataStruct.TABLE_NAME, GiocateLottoDataStruct._ID+"=?", new String[]{Long.toString(id)});
            result = n > 0;
		}
		catch (SQLiteException sqle){
			result = false;
		}
        finally {
			Log.d(TAG,"DB_CLOSE:");
            db.close();
        }
		return result;
	}
	
	/***
	 * Cancella tutte le giocate non pi? valide
	 * @param n di giocate valide
	 * @return
	 */
	public synchronized boolean deleteGamesAfter(int n){
		boolean result = false;
		long indexExt = DateUtils.getIndexNow() - n;
		SQLiteDatabase db=dbhelper.getWritableDatabase();
		try{
			int rDel = db.delete(GiocateLottoDataStruct.TABLE_NAME, 
					// "(?-"+GiocateLottoDataStruct.ID_EXT+")>?", 
					// "("+GiocateLottoDataStruct.ID_EXT+"-"+GiocateLottoDataStruct.N_GIOCATE_VALIDE+")>?", 
					GiocateLottoDataStruct.ID_EXT+"<=?",
					new String[]{Long.toString(indexExt)});
			if (rDel>0){
				Log.d(TAG,"trovate n:"+rDel+" da cancellare");
				result = true;
			}
			else{
				result = false;
			}
		}
		catch (SQLiteException sqle){
			result = false;
		}
		finally {
			Log.d(TAG,"DB_CLOSE:");
            db.close();
        }
		return result;
	}

	/***
	 * Elimina tutte le giocate che hanno gia' avuto una vincita massima
	 * @return
	 */
	public synchronized boolean deleteGamesMaxWin(){
		SQLiteDatabase db=dbhelper.getWritableDatabase();
		try{
			if (db.delete(GiocateLottoDataStruct.TABLE_NAME, GiocateLottoDataStruct.IS_MAX_WIN+"=?", new String[]{"1"})>0){
				Log.d(TAG,"deleteGamesMaxWin OK");
				return true;
			}
			return false;
		}
		catch (SQLiteException sqle){
			return false;
		}
		finally {
			Log.d(TAG,"DB_CLOSE:");
			db.close();
		}
	}

	private GiocateLottoDataItem _cursor2GiocataLottoDataItem(Cursor c) {
		GiocateLottoDataItem item = new GiocateLottoDataItem();
		item._id = c.getInt(c.getColumnIndex(GiocateLottoDataStruct._ID));
		item.data = c.getString(c.getColumnIndex(GiocateLottoDataStruct.DATA));
		item.id_ext = c.getInt(c.getColumnIndex(GiocateLottoDataStruct.ID_EXT));
		item.progressivoGiornaliero = c.getInt(c.getColumnIndex(GiocateLottoDataStruct.PG));
		item.numeriGiocati[0] = c.getInt(c.getColumnIndex(GiocateLottoDataStruct._1));
		item.numeriGiocati[1] = c.getInt(c.getColumnIndex(GiocateLottoDataStruct._2));
		item.numeriGiocati[2] = c.getInt(c.getColumnIndex(GiocateLottoDataStruct._3));
		item.numeriGiocati[3] = c.getInt(c.getColumnIndex(GiocateLottoDataStruct._4));
		item.numeriGiocati[4] = c.getInt(c.getColumnIndex(GiocateLottoDataStruct._5));
		item.numeriGiocati[5] = c.getInt(c.getColumnIndex(GiocateLottoDataStruct._6));
		item.numeriGiocati[6] = c.getInt(c.getColumnIndex(GiocateLottoDataStruct._7));
		item.numeriGiocati[7] = c.getInt(c.getColumnIndex(GiocateLottoDataStruct._8));
		item.numeriGiocati[8] = c.getInt(c.getColumnIndex(GiocateLottoDataStruct._9));
		item.numeriGiocati[9] = c.getInt(c.getColumnIndex(GiocateLottoDataStruct._10));
		item.numeroSpeciale   = c.getInt(c.getColumnIndex(GiocateLottoDataStruct.NS));
		item.extStat   		  = c.getLong(c.getColumnIndex(GiocateLottoDataStruct.EXT_STAT));
		item.ext_delta        = c.getInt(c.getColumnIndex(GiocateLottoDataStruct.EXT_DELTA));
		item.ext_diff 		  = c.getLong(c.getColumnIndex(GiocateLottoDataStruct.EXT_DIFF));
		item.vincita 	  	  = c.getInt(c.getColumnIndex(GiocateLottoDataStruct.WIN));
		item.vincitaTot 	  = c.getInt(c.getColumnIndex(GiocateLottoDataStruct.WIN_TOT));
		item.is_max_vincita   = c.getInt(c.getColumnIndex(GiocateLottoDataStruct.IS_MAX_WIN)) == 1;
		item.n_giocate_valide = c.getInt(c.getColumnIndex(GiocateLottoDataStruct.N_GIOCATE_VALIDE));
		return item;
	}
}