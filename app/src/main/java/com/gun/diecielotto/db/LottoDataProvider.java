package com.gun.diecielotto.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

public class LottoDataProvider extends ContentProvider{
	static final String AUTHORITY_NAME = "com.gun.diecielotto.db";

	private static final int FIND = 1;								// trova estrazione N per la data DDMMYYYY
	private static final int SAVE_ESTRAZIONE = 2;					// salva estrazione
	private static final int LOAD_ALL_EXTRACTIONS_FROM_DATE = 3;
	private static final int LOAD_LAST_N_EXTRACTIONS = 4;

	private DbManager db = null;
	static final UriMatcher uriMatcher;
	static final int uriCode = 1;

	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(AUTHORITY_NAME, "data/*/#", FIND);
		uriMatcher.addURI(AUTHORITY_NAME, "data/save", SAVE_ESTRAZIONE);
	}

	@Override
	public boolean onCreate() {
		this.db = DbManager.getInstance(getContext());
		return true;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	/***
	 * Implement this to handle requests for the MIME type of the data at the given URI.
	 * The returned MIME type should start with
	 * - vnd.android.cursor.item for --> single record,
	 * - vnd.android.cursor.dir/ for --> multiple items.
	 * This method can be called from multiple threads, as described in Processes and Threads.
	 * @param uri
	 * @return
	 */

	@Override
	public String getType(Uri uri) {
		switch (uriMatcher.match(uri)) {
			case FIND:
				return "vnd.android.cursor.item/ext";
			case SAVE_ESTRAZIONE:
				return "vnd.android.cursor.dir/ext";
			default:
				throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
	}

	@Override
	public Uri insert(Uri arg0, ContentValues arg1) {
		return null;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] args) {
		return 0;
	}

	// Qui vengono gestite le query a seconda dell'URI
	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		int res = uriMatcher.match(uri);
		switch (res) {
			case FIND:
				String date = uri.getPathSegments().get(1);
				String id = uri.getPathSegments().get(2);
				break;
			case SAVE_ESTRAZIONE:
				break;
		}
		return null;
	}


}