package com.gun.diecielotto.db;


/****
 * {"data":20160102,
 * "progressivoGiornaliero":11,
 * "massimoProgressivoGiornaliero":null,
 * "numeriEstratti":["4","6","8","19","22","35","39","43","48","56","62","66","68","69","75","76","77","83","88","89"],
 * "numeroSpeciale":8}
 * @author stefano
 */
public class LottoDataItem extends BaseDataItem{
	private static final String header = "_id,data,index,progressivoGiornaliero,massimoProgressivoGiornaliero,e1,e2,e3,e4,e5,e6,e7,e8,e9,e10,e11,e12,e13,e14,e15,e16,e17,e18,e19,e20,numeroSpeciale\n";  // serve x esport - appena riesco a farlo
	public boolean flg_h = false;
	
	public int massimoProgressivoGiornaliero;
	public int[] numeriEstratti = new int[20];
	public int numeroSpeciale;
	
	@Override
	public long getIndexExt(){
		return _id;
	}

	@Override
	public String toString() {
		if(flg_h){
			return header;
		}
		StringBuilder sb = new StringBuilder();
		for (int e : numeriEstratti) {
			sb.append(","+e);
		}
		String result = "ID:"+_id+",DATA:"+data+",PG:"+progressivoGiornaliero+",Ex:"+sb.substring(1)+",NS:"+numeroSpeciale+"\n";
		return result;
	}
}
