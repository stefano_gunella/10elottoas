package com.gun.diecielotto.db;

import android.provider.BaseColumns;

/****
 * {"data":20160101,
 * "progressivoGiornaliero":11,
 * "numeriGiocati":["1",...,"10"]
 * @author stefano
 */
public abstract class GiocateLottoDataStruct implements BaseColumns {
	public static final String TABLE_NAME = "giocate10elotto"; 	// non pu? cominciare per un numero.
	public static final String DATA = "data";
	public static final String ID_EXT = "id_ext";			// indice estrazione associata alla giocata
	public static final String PG = "progressivoGiornaliero";
	public static final String _1 = "e1";
	public static final String _2 = "e2";
	public static final String _3 = "e3";
	public static final String _4 = "e4";
	public static final String _5 = "e5";
	public static final String _6 = "e6";
	public static final String _7 = "e7";
	public static final String _8 = "e8";
	public static final String _9 = "e9";
	public static final String _10 = "e10";
	public static final String NS = "numeroSpeciale";
	public static final String EXT_STAT = "ext_stat";			// numero di estrazioni usate per la previsione
	public static final String EXT_DELTA = "ext_delta";			// delta numero di estrazioni usate per la previsione
	public static final String EXT_DIFF  = "ext_diff";			// delta numero di estrazioni attese per una vincita	
	public static final String WIN = "vincita";					// ultima vincita
	public static final String WIN_TOT = "vincita_tot"; 		// vincita Totale
	public static final String IS_MAX_WIN = "is_max_win"; 		// vincita massima realizzata
	public static final String N_GIOCATE_VALIDE = "n_giocate_valide"; 	// numero di volte in cui la giocata ? valida
}
