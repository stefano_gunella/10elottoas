package com.gun.diecielotto.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class LottoDbHelper extends SQLiteOpenHelper {
	// If you change the database schema, you must increment the database version.
	public static final int DATABASE_VERSION = 6;
	private static final String TAG = "LottoDbHelper";
	public static final String DATABASE_NAME = "lottoe10.db";// FIXME:questo DB deve essere aggiunto all'applicazione per non caricare i valori dall'inizio.
	private static final String SQL_CREATE_10ELOTTO =
			"CREATE TABLE " + LottoDataStruct.TABLE_NAME + " ("+LottoDataStruct._ID +" INTEGER PRIMARY KEY," +
					LottoDataStruct.DATA +" TEXT," + 
					LottoDataStruct._1   +" INTEGER," +
					LottoDataStruct._2   +" INTEGER," +
					LottoDataStruct._3   +" INTEGER," +
					LottoDataStruct._4   +" INTEGER," +
					LottoDataStruct._5   +" INTEGER," +
					LottoDataStruct._6   +" INTEGER," +
					LottoDataStruct._7   +" INTEGER," +
					LottoDataStruct._8   +" INTEGER," +
					LottoDataStruct._9   +" INTEGER," +
					LottoDataStruct._10  +" INTEGER," +
					LottoDataStruct._11  +" INTEGER," +
					LottoDataStruct._12  +" INTEGER," +
					LottoDataStruct._13  +" INTEGER," +
					LottoDataStruct._14  +" INTEGER," +
					LottoDataStruct._15  +" INTEGER," +
					LottoDataStruct._16  +" INTEGER," +
					LottoDataStruct._17  +" INTEGER," +
					LottoDataStruct._18  +" INTEGER," +
					LottoDataStruct._19  +" INTEGER," +
					LottoDataStruct._20  +" INTEGER," +
					LottoDataStruct.PG   +" INTEGER," +
					LottoDataStruct.MPG  +" INTEGER," +
					LottoDataStruct.NS   +" INTEGER)";
	
	private static final String SQL_CREATE_GIOCATE10ELOTTO =
			"CREATE TABLE " + GiocateLottoDataStruct.TABLE_NAME + " ("+GiocateLottoDataStruct._ID +" INTEGER PRIMARY KEY AUTOINCREMENT," +
					GiocateLottoDataStruct.DATA +" TEXT," +
					GiocateLottoDataStruct.ID_EXT  +" INTEGER," +
					GiocateLottoDataStruct._1   +" INTEGER," +
					GiocateLottoDataStruct._2   +" INTEGER," +
					GiocateLottoDataStruct._3   +" INTEGER," +
					GiocateLottoDataStruct._4   +" INTEGER," +
					GiocateLottoDataStruct._5   +" INTEGER," +
					GiocateLottoDataStruct._6   +" INTEGER," +
					GiocateLottoDataStruct._7   +" INTEGER," +
					GiocateLottoDataStruct._8   +" INTEGER," +
					GiocateLottoDataStruct._9   +" INTEGER," +
					GiocateLottoDataStruct._10  +" INTEGER," +
					GiocateLottoDataStruct.NS   +" INTEGER," +
					GiocateLottoDataStruct.PG   +" INTEGER," +
					GiocateLottoDataStruct.EXT_STAT  +" INTEGER," +
					GiocateLottoDataStruct.EXT_DELTA +" INTEGER," +
					GiocateLottoDataStruct.EXT_DIFF  +" INTEGER," +
					GiocateLottoDataStruct.WIN  +" INTEGER," +
					GiocateLottoDataStruct.WIN_TOT  +" INTEGER," +
					GiocateLottoDataStruct.IS_MAX_WIN  +" INTEGER,"+
					GiocateLottoDataStruct.N_GIOCATE_VALIDE  +" INTEGER);";

	private static final String SQL_DROP_LOTTO = "DROP TABLE IF EXISTS " + LottoDataStruct.TABLE_NAME +";";
	private static final String SQL_DROP_GAMES = "DROP TABLE IF EXISTS " + GiocateLottoDataStruct.TABLE_NAME+";";
	public LottoDbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.d(TAG, "onCreate DB");
		try{
			db.execSQL(SQL_CREATE_10ELOTTO);
			db.execSQL(SQL_CREATE_GIOCATE10ELOTTO);
		}
		catch(SQLException e){
			Log.d(TAG, "ERROR onCreate DB:"+e.getMessage());
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// This database is only a cache for online data, so its upgrade policy is
		// to simply to discard the data and start over
		Log.d(TAG, "onUpgrade - NO DROP TABLES");
		db.execSQL(SQL_DROP_LOTTO);
		db.execSQL(SQL_DROP_GAMES);
		onCreate(db);
	}

	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		onUpgrade(db, oldVersion, newVersion);
	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
	}
	/***
	 * Only for test
	 * @param db
	 */
	public void onDrop(SQLiteDatabase db) {
		db.execSQL(SQL_DROP_LOTTO);
		db.execSQL(SQL_DROP_GAMES);
	}
}