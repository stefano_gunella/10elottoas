package com.gun.diecielotto.db;

import java.util.Date;

import com.gun.diecielotto.util.DateUtils;

public abstract class BaseDataItem {
	public long _id;
	public String data;
	public long ext_delta;			// differenza rispetto al progressivo giornaliero - per simulare giocate precedenti
	public int progressivoGiornaliero;
	public long ext_diff;			// differenza tra 2 estrazioni
	public boolean is_max_vincita;	// nel caso di vincita, la massima possibile
	
	public abstract long getIndexExt();
	
	public Date getDate(){
		return DateUtils.stringFormatToDate(data);
	}
	public String getDateUI(){
		return DateUtils.getYYYYMMDD2UI(data);
	}
}
