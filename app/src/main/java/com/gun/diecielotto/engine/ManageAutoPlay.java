package com.gun.diecielotto.engine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.gun.diecielotto.DieciELottoActivity;
import com.gun.diecielotto.db.DbManager;
import com.gun.diecielotto.db.GiocateLottoDataItem;
import com.gun.diecielotto.db.LottoDataItem;
import com.gun.diecielotto.engine.data.BaseItems;
import com.gun.diecielotto.service.BroadcastObservable;
import com.gun.diecielotto.util.DateUtils;
import com.gun.diecielotto.util.GamesUtil;
import com.gun.diecielotto.util.file.FileManager;
import com.gun.diecielotto.view.SelectNumbersToPlayFragment;

public class ManageAutoPlay implements Runnable{
	private static final String TAG = "ManageAutoPlay";
	private static Integer ID = null;
	public static ManageAutoPlay instance = null;
	public static boolean flgReset = false;
	private DieciELottoActivity main = null;
	private Context context = null;
	private int num_max_ext = 0;									// numero massime giocate
	private int n_meno_estratti = 0 ; 								// 10,20...il numero minimo dei meno estratti
	private int n_offset_in_classifica = 0 ; 						// posizione di partenza in classifica
//	private int n_piu_estratti = 0 ; 								// 10,20...il numero minimo dei mai estratti
	private boolean sw_delete_max_play = false;
	private boolean sw_delete_games_win = false;
	private boolean sw_list_singoli_ambi = false; 					// true = AMBI, false = SINGOLI
	private boolean sw_giocate_gruppi = false;						// giocate a gruppi di n estratti
	private static HashMap<String,BaseItems> hashEstrazioni = null; // chiave hash es: 13-54
//	private static ArrayList<BaseItems> listaEstratti0Volte = new ArrayList<BaseItems>();	// creata a partire dalla tabella di hash
//	private static ArrayList<BaseItems> listaEstrattiNVolte = new ArrayList<BaseItems>();	// creata a partire dalla tabella di hash
	private static ArrayList<BaseItems> listaNEstrazioni = new ArrayList<BaseItems>();	// creata a partire dalla tabella di hash
	private static ArrayList<BaseItems> listaNEstrazioni_gruppi = new ArrayList<BaseItems>();	// creata a partire dalla tabella di hash
	Thread job = null;

	// SINGLETON
	private ManageAutoPlay(){
		ID = (int)(Math.random()*1000);
	}

	public static ManageAutoPlay getInstance(Context context){
		if(instance == null){
			instance = new ManageAutoPlay();
		}
		instance.context = context;
		return instance;
	}

	public void startAuto(boolean reset){
		if(job == null || !job.isAlive()){
			job = new Thread(this);
			SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
			num_max_ext = preferences.getInt("num_max_ext", 0);
			sw_delete_max_play = preferences.getBoolean("sw_delete_max_play", false);
			n_meno_estratti = preferences.getInt("n_meno_estratti", 0);
			n_offset_in_classifica = preferences.getInt("n_offset_in_classifica", 0);
			sw_delete_games_win = preferences.getBoolean("sw_delete_games_win", false);
			sw_list_singoli_ambi = preferences.getBoolean("sw_list_singoli_ambi", false);
			sw_giocate_gruppi  = preferences.getBoolean("sw_giocate_gruppi", false);
			// per adesso direi sempre RESET
			if(reset){
				flgReset = true;
				hashEstrazioni = null;
//				listaEstratti0Volte.clear();
//				listaEstrattiNVolte.clear();
				listaNEstrazioni.clear();
			}
			job.start();
		}
	}

	// ======================================================================
	// prima consideravo validi solo i numeri mai estratti, ma la stessa cosa
	// vale anche per quelli estratti 1,2,3,...n volte!
	// Proviamo con i numeri fino a 10 estrazioni - TODO:pensare come fare :)
	// ======================================================================
	@Override
	public void run() {
		DbManager db = DbManager.getInstance(this.context);
		if(sw_delete_max_play){
			db.deleteGamesAfter(num_max_ext);	// elimina i giochi dopo #n estrazioni
		}
		if(sw_delete_games_win){
			db.deleteGamesMaxWin();				// elimina i giochi che hanno avuto una vincita massima
		}
		if(null == hashEstrazioni){
			Log.d(TAG, "=== INITIALIZE LISTE ESTRAZIONE ===");
			FileManager.getInstance().log2File(context, "=== INITIALIZE LISTE ESTRAZIONE ===","ManageAutoplay.log");
			int gruppiElem = sw_list_singoli_ambi ? 2 : 1;
			initHashEstratti(gruppiElem);
			creaListaEstrattiUltimeNEstrazioni(GamesUtil.getInstance(context).getExtStat());
		}
		ArrayList<Integer> list2Play = null;
		Log.d(TAG, "sw_giocate_gruppi -->"+sw_giocate_gruppi);
		if(sw_giocate_gruppi){
			_giocaGruppiNumeriTrovati();
		}
		else {
			Log.d(TAG, "=== AGGIORNA LISTE DA GIOCARE ===");
			FileManager.getInstance().log2File(context, "=== AGGIORNA LISTE DA GIOCARE ===", "ManageAutoplay.log");
			list2Play = _giocaCombinazioniNumeriTrovati(listaNEstrazioni);
			_updateListaDaGiocare(list2Play);
		}
		FileManager.getInstance().log2File(context, "UPDATE giocate "+list2Play.size(),"ManageAutoplay.log");


		// aggiorna la lista delle giocate
		if(null != BroadcastObservable.getInstance()){
			BroadcastObservable.getInstance().updateAll(null);
		}
	}

	private void _updateListaDaGiocare(ArrayList<Integer> allNumbers) {
		if(context instanceof DieciELottoActivity){
			main = (DieciELottoActivity)context;
			for (Integer item : allNumbers) {
                main.listaDaGiocare.add(item);
			}
			SelectNumbersToPlayFragment sntpf = (SelectNumbersToPlayFragment)main.getFragmentManager().findFragmentByTag("numbers_2_play");
			if(sntpf!=null){
				sntpf.updateButtonsListaDaGiocare();	
			}
		}
	}

	/***
	 * Gioca tutti i gruppi di n numeri presenti in listaDaGiocare
	 * listaNEstrazioni contiene l'elenco delle estrazioni ordinate per n. di volte
	 */
	private void  _giocaGruppiNumeriTrovati() {
		Log.d(TAG, "_giocaGruppiNumeriTrovati");
		String n = GamesUtil.getInstance(context).getChkSelected("0");// default 0
		String[] aN = n.split("\\|");
		ArrayList<Integer> list2Play = new ArrayList<Integer>();
		BaseItems item = null;
		for (String l_gruppo : aN) {
			if (l_gruppo.length() > 0) {
				for (int i = 0; i < 90-Integer.parseInt(l_gruppo) ; i++) {
					List<BaseItems> listaDaGiocare = listaNEstrazioni_gruppi.subList(i, i + Integer.parseInt(l_gruppo));
					int[] game = GamesUtil.getInstance(context).listBaseItemToArray(listaDaGiocare);
					GiocateLottoDataItem g = GamesUtil.getInstance(context).createGame(game);
					DbManager.getInstance(context).saveGiocate(g);
				}
			}
		}
	}

	/***
	 * Gioca tutte le combinazioni di n numeri presenti in listaDaGiocare
	 * @return
	 */
	private ArrayList<Integer> _giocaCombinazioniNumeriTrovati(ArrayList<BaseItems> listaDaGiocare) {
		Log.d(TAG, "_giocaCombinazioniNumeriTrovati");
		String n = GamesUtil.getInstance(context).getChkSelected("0");// default 0
		String[] aN = n.split("\\|");
		ArrayList<Integer> list2Play = new ArrayList<Integer>();
		for (BaseItems item : listaDaGiocare) {
			GamesUtil.getInstance(context).add2ListNoDuplicate(list2Play,item.ext);
		}
		Log.d(TAG, "___ numeri da giocare:"+list2Play.size());
		int[] ldg = GamesUtil.getInstance(context).integerArray2Primitive(list2Play.toArray(new Integer[0]));
		for (String s : aN) {
			if(s.length()>0){
				GamesUtil.getInstance(context).calcolaCombinazioni(ldg, Integer.parseInt(s));
			}
		}
		return list2Play;
	}

//	private void _giocaAssiemeTuttiNumeriTrovati(ArrayList<Integer> allNumbers) {
//		int[] numeri_da_giocare = GamesUtil.getInstance(context).integerList2Primitive(allNumbers);
//		GiocateLottoDataItem giocata = GamesUtil.getInstance(context).createGame(numeri_da_giocare);
//		Log.d(TAG, "GIOCATA:"+giocata);
//		if(null != giocata){
//			DbManager.getInstance(context).saveGiocate(giocata);
//		}
//	}

//	private void _aggiungiUltimoEstratto() {
//		long id_ultima_estrazione = DateUtils.getIndexNow();
//		DbManager db = DbManager.getInstance(this.context);
//		LottoDataItem estrazione = db.getExtractionById(id_ultima_estrazione);
//		int gruppiElem = sw_list_singoli_ambi ? 2 : 1;
//		if(estrazione != null){
//			calcolaCombinazioniR(false, estrazione.numeriEstratti, gruppiElem, null, 0, 1, true);	// a gruppi di AMBI
//			_creaListaEstrattiEqualTo(listaEstratti0Volte, 0);
//			Log.d(TAG, "--- _aggiungiUltimoEstratto "+estrazione);
//		}
//	}

	/***
	 * Inizializza hashEstrazioni in modo che contenga tutti gli elementi con contatore a 0
	 */
	public void creaListaEstrattiUltimeNEstrazioni(int nEstrazioni) {
		DbManager db = DbManager.getInstance(this.context);
		int gruppiElem = sw_list_singoli_ambi ? 2 : 1;
		long id_ultima_estrazione = DateUtils.getIndexNow() - GamesUtil.getInstance(context).getExtDelta(); // considero anche il DELTA estrazioni

		for (long index = id_ultima_estrazione ; index > id_ultima_estrazione - nEstrazioni; index --) {
			LottoDataItem estrazione = db.getExtractionById(index);
			if(estrazione!=null ){
				// lavora su hashEstrazioni
				calcolaCombinazioniR(false, estrazione.numeriEstratti, gruppiElem, null, 0, 1, true);	// a gruppi di AMBI
				Log.d(TAG, index+ "--> estrazione ID:"+index);
			}
			else{
				Log.d(TAG, "--- MANCA ---"+index);
			}
		}
		_creaListaEstrattiOrdinata(listaNEstrazioni, n_meno_estratti, n_offset_in_classifica);
		_creaListaEstrattiOrdinata(listaNEstrazioni_gruppi,90, 0);
//		Log.d(TAG, "--- inizializza ultimi estratti:"+listaNEstrazioni_gruppi.size());
	}

	/***
	 * Inizializza hashEstrazioni in modo che contenga tutti gli elementi con contatore a 0
	 */
	@Deprecated
//	public void creaListaEstratti0VolteEMaxVolte() {
//		initializeMaiEstratti = false;
//		DbManager db = DbManager.getInstance(this.context);
//		int index = 0;
//		int gruppiElem = sw_list_singoli_ambi ? 2 : 1;
//		long id_ultima_estrazione = DateUtils.getIndexNow() - GamesUtil.getInstance(context).getExtDelta(); // considero anche il DELTA estrazioni
//		int numeroMaiEstrattiCorrenti = _ricalcolaNumeroEstrattiPer(0);
//		while (numeroMaiEstrattiCorrenti > n_meno_estratti) {
//			// 1. creare metodo ricorsivo per calcolare i gruppi di numeri mai estratti(ambi, terni, ecc)
//			LottoDataItem estrazione = db.getExtractionById(id_ultima_estrazione);
//			if(estrazione!=null){
//				calcolaCombinazioniR(false, estrazione.numeriEstratti, gruppiElem, null, 0, 1, true);	// a gruppi di AMBI
//				// 2. si deve fermare quando ha trovato almeno n_meno_estratti
//				// 3. una volta trovati deve creare le giocate corrispondenti
//				// 4. deve aggiungere le giocate a quelle in corso
//				numeroMaiEstrattiCorrenti = _ricalcolaNumeroEstrattiPer(0);
//				Log.d(TAG, index+ "--> init mai estatti:"+numeroMaiEstrattiCorrenti);
//			}
//			else{
//				Log.d(TAG, "--- MANCA ---"+id_ultima_estrazione);
//			}
//			id_ultima_estrazione--;
//			index++ ;
//		}
//		_creaListaEstrattiEqualTo(listaEstratti0Volte,0);
//		_creaListaEstrattiUpTo(listaEstrattiNVolte);
//		Log.d(TAG, "--- inizializza mai estratti:"+listaEstrattiNVolte.size());
//	}

//	private void _updateListaEstratti(int nVolte) {
//		DbManager db = DbManager.getInstance(this.context);
//		int estrazioniMancanti = 0;// conta le estrazioni assenti...
//		int index = 0;
//		int gruppiElem = sw_list_singoli_ambi ? 2 : 1;
//		int numeroEstrattiNVolte = _ricalcolaNumeroEstrattiPer(nVolte);
//		long id_estrazione_ricalcolo = DateUtils.getIndexNow() - GamesUtil.getInstance(context).getExtDelta();
//		if(numeroEstrattiNVolte < n_meno_estratti){
//			// deve diminuire le estrazioni prese in considerazione
//			// mi sposto verso destra
//			while (numeroEstrattiNVolte < n_meno_estratti) {
//				// 1. creare metodo ricorsivo per calcolare i gruppi di numeri mai estratti(ambi, terni, ecc)
//				LottoDataItem estrazione = db.getExtractionById(id_estrazione_ricalcolo+1);
//				if(estrazione!=null){
//					calcolaCombinazioniR(false, estrazione.numeriEstratti, gruppiElem, null, 0, 1, false);	// a gruppi di AMBI
//					// 2. si deve fermare quando ha trovato almeno n_meno_estratti
//					// 3. una volta trovati deve creare le giocate corrispondenti
//					// 4. deve aggiungere le giocate a quelle in corso
//					numeroEstrattiNVolte = _ricalcolaNumeroEstrattiPer(nVolte);
//					Log.d(TAG, index+ "--> ricalcolo estratti #"+nVolte+" volte = " + numeroEstrattiNVolte);
//				}
//				else{
//					Log.d(TAG, "---MANCA DX---"+id_estrazione_ricalcolo);
//					estrazioniMancanti++;
//				}
//				id_estrazione_ricalcolo++;
//				index++ ;
//				if(estrazioniMancanti > 10){
//					break;
//				}
//			}
//		}
//		else if(numeroEstrattiNVolte > n_meno_estratti){
//			// deve aumentare le estrazioni prese in considerazione
//			// mi sposto verso sinista
//			while (numeroEstrattiNVolte > n_meno_estratti) {
//				// 1. creare metodo ricorsivo per calcolare i gruppi di numeri mai estratti(ambi, terni, ecc)
//				LottoDataItem estrazione = db.getExtractionById(id_estrazione_ricalcolo-1);
//				if(estrazione!=null){
//					calcolaCombinazioniR(false, estrazione.numeriEstratti, gruppiElem, null, 0, 1, true);		// a gruppi di AMBI
//					// 2. si deve fermare quando ha trovato almeno n_meno_estratti
//					// 3. una volta trovati deve creare le giocate corrispondenti
//					// 4. deve aggiungere le giocate a quelle in corso
//					numeroEstrattiNVolte = _ricalcolaNumeroEstrattiPer(nVolte);
//					Log.d(TAG, index+ "--> ricalcolo estratti #"+nVolte+" volte = " + numeroEstrattiNVolte);
//				}
//				else{
//					Log.d(TAG, "---MANCA SX---"+id_estrazione_ricalcolo);
//					estrazioniMancanti++;
//				}
//				id_estrazione_ricalcolo--;
//				index++ ;
//				if(estrazioniMancanti > 10){
//					break;
//				}
//			}
//		}
//		_creaListaEstrattiEqualTo(listaEstratti0Volte, nVolte);
//		_creaListaEstrattiUpTo(listaEstrattiNVolte);
//		Log.d(TAG, "_updateListaEstratti "+index+":"+listaEstrattiNVolte.size());
//	}

	// ricalcola i numeri estratti n volte
	// i mai estratti sono quelli con 0 :)
	private int _ricalcolaNumeroEstrattiPer(int x) {
		int n=0;
		for (BaseItems item : hashEstrazioni.values()) {
			n = item.tot == x ? n+1 : n;
		}
		return n;
	}

	/***
	 * Aggiunge alla lista indicata solo gli elementi (singoli,ambi) estratti nVolte
	 * @param nVolte
	 */
//	private void _creaListaEstrattiEqualTo(ArrayList<BaseItems> listaTarget, int nVolte) {
//		listaTarget.clear(); // resetto la lista target
//		ArrayList<BaseItems> targetCollection = new ArrayList<BaseItems>(hashEstrazioni.values());
//		Collections.sort(targetCollection);
//		BaseItems item = null;
//		int sizeTarget = targetCollection.size()-1;
//		for (int i=0; i < n_meno_estratti; i++) {
//			item = targetCollection.get(i);
//			if(item.tot == nVolte) {
//                listaTarget.add(item);
//                Log.d(TAG, "---MENO ESTRATTI:[#" + item.tot + ":" + item.toString() + "]");
//            }
//		}
//	}

	/***
	 * Aggiunge alla lista indicata solo gli elementi (singoli,ambi) estratti nVolte
	 */
//	private void _creaListaEstrattiUpTo(ArrayList<BaseItems> listaTarget) {
//		listaTarget.clear(); // resetto la lista target
//		ArrayList<BaseItems> targetCollection = new ArrayList<BaseItems>(hashEstrazioni.values());
//		Collections.sort(targetCollection);
//		BaseItems item = null;
//		int sizeTarget = targetCollection.size()-1;
//		for (int i=0; i < n_piu_estratti; i++) {
//			item = targetCollection.get(sizeTarget-i);
//			if(!item.isUltimaEstrazione) {
//				listaTarget.add(item);
//				Log.d(TAG, "---PIU ESTRATTI:[#" + item.tot + ":" + item.toString() + "]");
//			}
//		}
//	}

	/***
	 * Aggiunge alla lista indicata solo gli elementi (singoli,ambi) delle ultime N estrazioni
	 * Di queste N estrazioni prende i MENO frequenti che non sono stati estratti l'ultima volta
	 */
	private void _creaListaEstrattiOrdinata(ArrayList<BaseItems> listaTarget, int _n_meno_estratti, int _n_offset_in_classifica) {
		listaTarget.clear(); // resetto la lista target
		ArrayList<BaseItems> targetCollection = new ArrayList<BaseItems>(hashEstrazioni.values());
		Collections.sort(targetCollection); // ordina dal meno estratto al piu estratto
		BaseItems item = null;
		int sizeTarget = targetCollection.size()-1;
		for (int i=0; i < _n_meno_estratti; i++) {
			item = targetCollection.get(i+_n_offset_in_classifica);
			if(!item.isUltimaEstrazione) {	// non lo considera se era presente nell'ultima estrazione
				listaTarget.add(item);
				Log.d(TAG, "---Ultimo ESTRATTO:[#" + item.tot + ":" + item.toString() + "]");
			}
		}
		Log.d(TAG, "---lista size:" + listaTarget.size() );
	}
	/***
	 * Inizializza hashTable per il calcolo delle statistiche...
	 * @param gruppiElem
	 */
	public void initHashEstratti(int gruppiElem) {
		hashEstrazioni = new HashMap<String,BaseItems>();
		BaseItems estrazioni = new BaseItems(90); 	// deve trovare tutte le combinazioni di 90 elementi
		estrazioni.init();								// inizializza gli elementi(in questo caso tutti i valori 1-90)
		calcolaCombinazioniR(true, estrazioni.ext, gruppiElem, null, 0, 1, true);	// a gruppi di nEstrazioni
	}

	/***
	 * Metodo Ricorsivo che estrae tutte le combinazioni di N-elementi di classe C
	 * @param estrazioni	: array dei numeri da combinare (N-elementi)
	 * @param gruppiElem	: 1:singoli, 2:ambi, 3:terni, ecc (la CLASSE C)
	 * @param item			: struttura elementi da giocare
	 * @param level			: livello ricorsione
	 * @flg_add_remove		: flg true=add, false=remove agginge o rimuove dall'hashEstrazioni
	 * 						  aggiunge se deve inizializzare l'array o inserire un nuovo elemente
	 * 						  rimuove quando calcola a ritroso le estrazioni per averle sempre a 0
	 */
	private void calcolaCombinazioniR(boolean initHash, int[] estrazioni, int gruppiElem, int[] item, int index, int level, boolean flg_add_remove) {
		int length = estrazioni.length;
		if(null == item){
			item = new int[gruppiElem];
		}
		for(int i=index; i<length - gruppiElem + level; i++){
			item[level-1] = estrazioni[i];
			// se e' l'ultimo livello inserisce nell'hash
			if(level == gruppiElem){
				BaseItems currentItem = new BaseItems(item.clone());
				BaseItems existItem = hashEstrazioni.get(currentItem.toString());
				if(null == existItem){
					currentItem.tot = initHash ? 0:1;
					hashEstrazioni.put(currentItem.toString(), currentItem);
					Log.d(TAG, "NEW["+currentItem+"]"+currentItem.tot);
				}
				else{
					if(flg_add_remove){
						existItem.tot++;
					}
					else{
						existItem.tot--;
					}
				}
			}
			// se non e' l'ultimo livello prosegue a quello successivo...
			if(level < gruppiElem){
				calcolaCombinazioniR(initHash, estrazioni, gruppiElem, item, i+1, level+1, flg_add_remove);
			}
		}
	}
}
