package com.gun.diecielotto.engine.data;

import com.gun.diecielotto.util.DateUtils;

import java.util.Calendar;

public class GiornoEdEstrazione {
    public Calendar day = null;
    public int numEstrazione = 0;
    public boolean isOK = false;

    public GiornoEdEstrazione(Calendar day,int numEstrazione){
        this.day = day;
        this.numEstrazione = numEstrazione;
    }

    @Override
    public String toString() {
        return "day:" + DateUtils.dateToStringFormatUI(this.day.getTime()) + " ext:" + numEstrazione;
    }
}
