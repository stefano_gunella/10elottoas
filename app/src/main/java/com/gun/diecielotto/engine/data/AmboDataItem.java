package com.gun.diecielotto.engine.data;

public class AmboDataItem extends BaseItems{
	public int ext_1 = 0;
	public int ext_2 = 0;

	public SingoloEstrattoDataItem getExt1(){
		SingoloEstrattoDataItem result = new SingoloEstrattoDataItem();
		result.ext = ext_1;
		result.tot = tot;
		return result;
	}
	
	public SingoloEstrattoDataItem getExt2(){
		SingoloEstrattoDataItem result = new SingoloEstrattoDataItem();
		result.ext = ext_2;
		result.tot = tot;
		return result;
	}
	
	@Override
	public String toString() {
		return ext_1+"-"+ext_2;
	}

	@Override
	public SingoloEstrattoDataItem[] getAllExt() {
		SingoloEstrattoDataItem[] result = new SingoloEstrattoDataItem[2];
		result[0] = getExt1();
		result[1] = getExt2();
		return result;
	}
}