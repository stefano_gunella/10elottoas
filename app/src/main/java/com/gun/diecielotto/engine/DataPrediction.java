package com.gun.diecielotto.engine;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.gun.diecielotto.DieciELottoActivity;
import com.gun.diecielotto.R;
import com.gun.diecielotto.db.DbManager;
import com.gun.diecielotto.db.GiocateLottoDataItem;
import com.gun.diecielotto.db.LottoDataItem;
import com.gun.diecielotto.engine.data.AmboDataItem;
import com.gun.diecielotto.engine.data.GiornoEdEstrazione;
import com.gun.diecielotto.engine.data.SingoloEstrattoDataItem;
import com.gun.diecielotto.service.BroadcastObservable;
import com.gun.diecielotto.util.DateUtils;
import com.gun.diecielotto.util.GamesUtil;
import com.gun.diecielotto.util.file.FileManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import androidx.core.app.NotificationCompat;

// import android.support.v4.app.NotificationCompat;

public class DataPrediction{
	final static String TAG = "DataPrediction";
	DbManager dbManager = null;
	GamesUtil gamesUtil = null;
	Context context = null;
	private int totaleEstrazioni = 0;
	static private int indexNotifications = 0;
	HashMap<String, SingoloEstrattoDataItem> hashEstrazioni = new HashMap<String, SingoloEstrattoDataItem>();
	HashMap<String, AmboDataItem> 				   hashAmbi = new HashMap<String, AmboDataItem>();
	private static List<GiornoEdEstrazione> listaEstrazioniFallite = new ArrayList<GiornoEdEstrazione>();
	public boolean stopPredicton = false;

	public DataPrediction(Context context){
		this.context = context;
		this.dbManager = DbManager.getInstance(context);
		this.gamesUtil = GamesUtil.getInstance(context);
	}
	
	public int getTotaleEstrazioni(){
		return totaleEstrazioni;
	}
	
	public List<AmboDataItem> getAmbiRicorrenti(){
		ArrayList<AmboDataItem> lresult = new ArrayList<AmboDataItem>(hashAmbi.values());
		Collections.sort(lresult,new Comparator<AmboDataItem>() {
			@Override
			public int compare(AmboDataItem i1, AmboDataItem i2) {
				if(i2.tot == i1.tot){
					if(i2.ext_1 == i1.ext_1){
						return i2.ext_2 - i1.ext_2;
					}
					return i2.ext_1 - i1.ext_1;
				}
				return i2.tot - i1.tot;
			}
		});
		return lresult;
	}
	
	public List<SingoloEstrattoDataItem> getEstrattiRicorrenti(){
		ArrayList<SingoloEstrattoDataItem> lresult = new ArrayList<SingoloEstrattoDataItem>(hashEstrazioni.values());
		Collections.sort(lresult, new Comparator<SingoloEstrattoDataItem>() {
			@Override
			public int compare(SingoloEstrattoDataItem i1, SingoloEstrattoDataItem i2) {
				return i2.tot - i1.tot;
			}
		});
		return lresult;
	}

	/***
	 * Calcola le statitiche considerando  il numero di estrazioni "extStat" con delta "extDelta"
	 * @param extStat - numero estrazioni per statistiche
	 * @param extDelta - n di estrazioni da saltare per il calcolo delle statistiche
	 */
	public void calcolaStatistiche(Integer extStat, Integer extDelta){
		counter = 0;
		_initSingoloEstrazioni();
		_initAmbiEstrazioni();
		List<LottoDataItem> lExtractions = DbManager.getInstance(context).loadLastNExtractions(extStat,extDelta);
		LottoDataItem lastItem = this.dbManager.getLastExtractionNow();
		if(lExtractions.size() > 0){
			for (LottoDataItem item : lExtractions) {
				if(stopPredicton){
					break; // EXIT
				}
				totaleEstrazioni++;
				_calcolaSingoloEstrazioni(item);
				_calcolaAmbiEstrazioni(item);
			}
			if(null!=lastItem){
				_segnaSingoliEstrattiUltimaEstrazione(lastItem);
				_segnaAmbiEstrattiUltimaEstrazione(lastItem);
			}
		}
	}

	public static Thread getThreadVerificaGiocate(final Context ctx, boolean resetTimer){
		Thread checkVincite = new Thread(){
			private int ID_THRED = 0;
			Set<GiocateLottoDataItem> setGiocateVincenti = null;
			List<GiornoEdEstrazione> _listaEstrazioniFallite = new ArrayList<GiornoEdEstrazione>();
			GamesUtil gu = GamesUtil.getInstance(ctx);

			@Override
		    public void run() {
				if (resetTimer){
					BroadcastObservable.getInstance().updateAll(5L*60L*1000L);
				}

				ID_THRED = GamesUtil.getInstance(ctx).getIdThred();
				setGiocateVincenti = new HashSet<GiocateLottoDataItem>();
				LottoDataItem ultimaEstrazione = null;
				long start_time = DateUtils.getNow();
				long now_time = 0;
				Calendar startingDay = Calendar.getInstance();
				String message = null;
				// continua finche non trova l'estrazione e sono passati meno di 10 secondi.
				while(ultimaEstrazione==null && (now_time - start_time) < 60000){
					ultimaEstrazione = PostTask.scaricaEstrazioneNumero(ctx, startingDay, null, true, false);
					message = "ID_THRED:"+ID_THRED+" time="+(now_time - start_time) + " scarico estrazione:" +ultimaEstrazione;
					FileManager.getInstance().log2File(ctx, message,"DataPrediction.log");
					now_time = DateUtils.getNow();
				}
				if(null != ultimaEstrazione) {
					List<GiocateLottoDataItem> listaGiocate = DbManager.getInstance(ctx).getListGiocate();
					if (gu.getImportoDaGiocare() >= listaGiocate.size()) {
						int vincitaTot = _checkVincite(ctx, ultimaEstrazione);
						Log.d(TAG, "_checkVincite="+vincitaTot);
						if (vincitaTot > 0) {
							// _getCounter(context);
							_alarm(ctx, ultimaEstrazione, vincitaTot, listaGiocate.size());
							gu.updateTotaleVincita(vincitaTot);
							// DieciELottoActivity.incrementBadge(ctx);// fixme: TEST
						}
						gu.updateTotaleGiocate(listaGiocate.size());
						gu.updateImportoDaGiocare(listaGiocate.size());
					}
					// questo aggiorna la vista delle giocate...
					if (null != BroadcastObservable.getInstance()) {
						BroadcastObservable.getInstance().updateAll(null);
					}
				}
				else{
					// TODO: aggiungo ad una lista gli scarti e li riprendo
					GiornoEdEstrazione ge = new GiornoEdEstrazione(startingDay,DateUtils.getNumeroEstrazioniDelGiorno(null));
					listaEstrazioniFallite.add(ge);
					FileManager.getInstance().log2File(ctx,"N. elementi:"+listaEstrazioniFallite.size()+" Aggiunto lista scarti:"+ge,"DataPrediction.log");
				}
				LottoDataItem estrazioneFallita = null;
				for (GiornoEdEstrazione item:listaEstrazioniFallite) {
					FileManager.getInstance().log2File(ctx, "Recupero estrazione FALLITA:"+item,"DataPrediction.log");
					estrazioneFallita = PostTask.scaricaEstrazioneNumero(ctx, item.day, item.numEstrazione, false, false);
					if(estrazioneFallita == null){
						_listaEstrazioniFallite.add(item);
					}
				}
				listaEstrazioniFallite.clear();
				listaEstrazioniFallite.addAll(_listaEstrazioniFallite);
				ManageAutoPlay.getInstance(ctx).startAuto(true);
			}

			private int _checkVincite(Context context, LottoDataItem estrazione){
				Log.d(TAG, "_checkVincite");
				int vincitaTot = 0;
				if(estrazione!=null){
					vincitaTot = gu.controllaVincite(estrazione, setGiocateVincenti);
				}
				return vincitaTot;
			}

			private void _alarm(Context context, LottoDataItem ultimaEstrazione, int vincita, int nGiocate) {
				indexNotifications ++;
				Uri uri_raw_ding = Uri.parse("android.resource://"+context.getPackageName()+"/"+R.raw.ding);
				Uri uri_raw_coin = Uri.parse("android.resource://"+context.getPackageName()+"/"+R.raw.coin);
				Uri uri_raw = (vincita >= nGiocate) ? uri_raw_coin: uri_raw_ding;
				NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
				inboxStyle.setBigContentTitle("Estrazioni vincenti");
				for (int i=0; i < setGiocateVincenti.size(); i++) {
					GiocateLottoDataItem g = (GiocateLottoDataItem)setGiocateVincenti.toArray()[i];
					inboxStyle.addLine(g.toString());
				}
				Log.d(TAG, "*** Estrazione "+ ultimaEstrazione.progressivoGiornaliero + " - TOTALE vincita:"+vincita+" ***");
				inboxStyle.setSummaryText("*** Estrazione "+ ultimaEstrazione.progressivoGiornaliero + " - TOTALE vincita:"+vincita+" ***");
				String CHANNEL_ID = "10elotto";// The id of the channel.
				NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context,CHANNEL_ID)
															.setStyle(inboxStyle)
															.setSmallIcon(R.drawable.icon_10elotto)
															.setContentTitle(indexNotifications+" - HAI VINTO "+vincita+" giocando "+nGiocate)
															.setContentText("vincita:"+vincita+" giocando "+nGiocate)
															.setSound(uri_raw)
															.setNumber(indexNotifications);
				Intent resultIntent = new Intent(context, DieciELottoActivity.class);
				PendingIntent resultPendingIntent = PendingIntent.getActivity(
						context,
						0,
						resultIntent,
						PendingIntent.FLAG_UPDATE_CURRENT);
				mBuilder.setContentIntent(resultPendingIntent);

				NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				// Don't forget to set the ChannelID!!
				if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
					Log.d(TAG, "*** NotificationChannel ***");
					NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
							"Channel for 10elotto",
							NotificationManager.IMPORTANCE_DEFAULT);
					channel.setDescription("PROVA NOTIFICA");
					channel.enableVibration(true);
					notificationManager.createNotificationChannel(channel);
				}
				notificationManager.notify(indexNotifications, mBuilder.build());
			}
		};
		return checkVincite;
	}
	
	/***
	 * Inizializza lista con tutti i possibili singoli estratti
	 */
	private void _initSingoloEstrazioni() {
		SingoloEstrattoDataItem singolo = null;
		for (int i = 1; i<=90; i++) {
			singolo = new SingoloEstrattoDataItem();
			singolo.ext = i;
			singolo.dtEstrazione = Calendar.getInstance().getTime();
			hashEstrazioni.put(""+i, singolo);
		}
	}
	
	/***
	 * Inizializza lista con tutti i possibili ambi estratti
	 */
	private void _initAmbiEstrazioni() {
		AmboDataItem ambo = null;
		for (int i = 1; i<90; i++) {
			for (int j = i+1; j<=90; j++) {
				ambo = new AmboDataItem();
				ambo.ext_1 = i;
				ambo.ext_2 = j;
				ambo.dtEstrazione = Calendar.getInstance().getTime();
				hashAmbi.put(ambo.ext_1+"-"+ambo.ext_2, ambo);
			}
		}
	}

	/***
	 * Conta le volte che un sigolo numero ? stato estratto nelle N estrazioni selezionate.
	 * @param item
	 */
	private void _calcolaSingoloEstrazioni(LottoDataItem item) {
		SingoloEstrattoDataItem singoloHash = null;
		SingoloEstrattoDataItem singolo = null;
		for (int i = 0; i<item.numeriEstratti.length; i++) {
			singolo = new SingoloEstrattoDataItem();
			singolo.ext = item.numeriEstratti[i];
			singoloHash = hashEstrazioni.get(""+singolo.ext);
			if(singoloHash==null){
				if(singolo.ext>0){
					Log.d(TAG, "SINGOLO:"+singolo);
					hashEstrazioni.put(""+singolo.ext,singolo);
				}
				singoloHash = singolo;
			}
			singoloHash.dtEstrazione = item.getDate();
			singoloHash.progressivoGiornaliero = item.progressivoGiornaliero;
			singoloHash.tot++;
		}
	}
	
	int counter = 0;
	private void _calcolaAmbiEstrazioni(LottoDataItem item){
		AmboDataItem ambo = null;
		AmboDataItem amboHash = null;
		for (int i = 0; i<item.numeriEstratti.length - 1; i++) {
			for (int j = i+1; j<item.numeriEstratti.length; j++) {
				ambo = new AmboDataItem();
				ambo.ext_1 = Math.min(item.numeriEstratti[i], item.numeriEstratti[j]);
				ambo.ext_2 = Math.max(item.numeriEstratti[i], item.numeriEstratti[j]);
				amboHash = hashAmbi.get(ambo.ext_1+"-"+ambo.ext_2);
				if(amboHash==null){
					if(ambo.ext_1 > 0 && ambo.ext_2 > 0){
						Log.d(TAG, "AMBO:"+ambo);
						hashAmbi.put(ambo.ext_1+"-"+ambo.ext_2, ambo);
					}
					amboHash = ambo;
				}
				amboHash.dtEstrazione = item.getDate();
				amboHash.progressivoGiornaliero = item.progressivoGiornaliero;
				amboHash.tot++;
			}
		}
	}

	private void _segnaSingoliEstrattiUltimaEstrazione(LottoDataItem item) {
		SingoloEstrattoDataItem singoloHash = null;
		if(item == null || null == item.numeriEstratti){
			return;
		}
		for (int i = 0; i<item.numeriEstratti.length; i++) {
			singoloHash = hashEstrazioni.get(""+item.numeriEstratti[i]);
			singoloHash.isUltimaEstrazione = true;
		}
	}
	
	/***
	 * Segno le coppie di ambi dell'ultima estrazione
	 * @param item
	 */
	private void _segnaAmbiEstrattiUltimaEstrazione(LottoDataItem item) {
		AmboDataItem amboHash = null;
		for (int i = 0; i<item.numeriEstratti.length - 1; i++) {
			for (int j = i+1; j<item.numeriEstratti.length; j++) {
				amboHash = hashAmbi.get(item.numeriEstratti[i]+"-"+item.numeriEstratti[j]);
				amboHash.isUltimaEstrazione = true;
			}
		}
	}
}
