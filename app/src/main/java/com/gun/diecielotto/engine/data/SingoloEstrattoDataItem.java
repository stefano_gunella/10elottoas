package com.gun.diecielotto.engine.data;

import java.util.Date;


public class SingoloEstrattoDataItem extends BaseItems{
	public int ext = 0; 

	@Override
	public String toString() {
		return ""+ext;
	}

	@Override
	public SingoloEstrattoDataItem[] getAllExt() {
		SingoloEstrattoDataItem[] result = new SingoloEstrattoDataItem[1];
		result[0] = this;
		return result;
	}
}