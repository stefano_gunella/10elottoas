package com.gun.diecielotto.engine.data;

import java.util.ArrayList;
import java.util.Date;

public class BaseItems implements Comparable<BaseItems>{
	public int nExt = 0;		// numero estrazioni 1:singolo, 2:ambi, 3:terno, ecc...
	public int[] ext = null;	// array estrazioni
	public int tot = 0;			// totale estrazioni
	public Date dtEstrazione = null;
	public int progressivoGiornaliero = 0;
	// public int estrazioniConsecutive = 0;
	// public int maxEstrazioniConsecutive = 0;
	public boolean isUltimaEstrazione = false;

	public SingoloEstrattoDataItem[] getAllExt(){
		return null; // override
	}

	public BaseItems(){
		ext = new int[1];
	}

	/***
	 * Costruttore per n valori;
	 * @param n
	 */
	public BaseItems(int n){
		this.ext = new int[n];
	}
	
	public BaseItems(int[] ext){
		this.ext = ext;
	}

	// usato per inizilizzare tutte le possibili giocate
	public void init(){
		for (int i=0;i<ext.length;i++) {
			ext[i]=i+1;
		}
	}
	
	public ArrayList<Integer> array2List(){
		ArrayList<Integer> lres= new ArrayList<Integer>();
		for (int i=0;i<ext.length;i++) {
			lres.add(ext[i]);
		}
		return lres;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (int i=0;i<ext.length;i++) {
			sb.append("-"+ext[i]);
		}
	return sb.toString().substring(1);
	}

	@Override
	public int compareTo(BaseItems another) {
		return this.tot - another.tot;
	}
}