package com.gun.diecielotto.engine;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Message;
import android.util.Log;

import com.gun.diecielotto.DieciELottoActivity;
import com.gun.diecielotto.db.DbManager;
import com.gun.diecielotto.db.LottoDataItem;
import com.gun.diecielotto.util.DateUtils;
import com.gun.diecielotto.util.GamesUtil;
import com.gun.diecielotto.util.file.FileManager;

public class PostTask implements Runnable{
	DbManager db = null;
	Context context = null;
	private int ID_THRED;
	final String TAG = "PostTask";
	final int MAX_RETRY = 3;
	private static Header[] headers = {
			new BasicHeader("Accept", "application/json"),
			new BasicHeader(HTTP.CONTENT_TYPE, "application/json"),
			new BasicHeader(HTTP.CONTENT_ENCODING,"application/json")
	};
	public static int numOfConnectTimeoutException = 0;

	private Calendar day = null;
	private int progressivoGiornaliero = 0;
	private boolean notify2Activity = true;
	public LottoDataItem lastItemInserted = null;

	public PostTask(Context context){
		this.context = context;
		ID_THRED = GamesUtil.getInstance(context).getIdThred();
		db = DbManager.getInstance(context);
		FileManager.getInstance().log2File(context, "GET DATA "+ ID_THRED,"PostTask.log");
	}

	public void init(Calendar day, int progressivoGiornaliero, boolean notify2Activity){
		Log.d(TAG, "=== task:" + progressivoGiornaliero);
		this.day = day != null ? day : Calendar.getInstance();
		this.progressivoGiornaliero = progressivoGiornaliero;
		this.notify2Activity = notify2Activity;
	}

	@SuppressLint("SimpleDateFormat")
	@Override
	public void run(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		String dataFormat = dateFormat.format(day.getTime()); // "20160129"
		boolean data_found = false;
		int numRetry = 0;
		String message = null;
		do {
			numRetry ++;
			message = "LOAD DATA - tentativo #"+numRetry +" prog:"+progressivoGiornaliero;
			FileManager.getInstance().log2File(context, message,"PostTask.log");
			data_found = _postJson(dataFormat, progressivoGiornaliero, true, numRetry);
			if (!data_found && numRetry >= MAX_RETRY) {
				message = "ATTENZIONE:dato non disponibile, riprovare piu' tardi";
				_sendMessage2UI(2, message);
				//FileManager.getInstance().log2File(context, message,"PostTask.log");
				data_found = true;
			}
		}
		while (!data_found); // continuo fiche' non ho trovato il dato...
		message = "RUN - dato :"+(lastItemInserted == null ? "ASSENTE" : "PRESENTE");
		FileManager.getInstance().log2File(context, message,"PostTask.log");
	}

	private boolean _postJson(String data, int progressivoGiornaliero, boolean testDataExist, int numRetry){
		LottoDataItem itemOnDb = db.find(data, progressivoGiornaliero);
		boolean is_data_available = false;
		if(testDataExist == true && itemOnDb!=null && itemOnDb.progressivoGiornaliero != 0){
			Log.d(TAG, "= TROVATO ("+progressivoGiornaliero+"):"+itemOnDb.toString());
			_sendMessage2UI(1,itemOnDb.toString());
			lastItemInserted = itemOnDb;
			is_data_available = true;
			return is_data_available;
		}
		else {
			HttpClient client = _createHttpClient();
			HttpConnectionParams.setConnectionTimeout(client.getParams(), 500); //Timeout Limit
			HttpResponse response = null;

			String read = null;
			try {
				// POST: {"data":"20160129","progressivoGiornaliero":"11"}
				HttpPost post = new HttpPost("https://www.lottomaticaitalia.it/del/estrazioni-e-vincite/10-e-lotto-estrazioni-ogni-5.json");
				JSONObject json = new JSONObject();
				json.put("data", data);
				json.put("progressivoGiornaliero", progressivoGiornaliero);
				StringEntity entity = new StringEntity(json.toString(), HTTP.UTF_8);
				post.setEntity(entity);

				post.setHeaders(headers);
				response = client.execute(post);
				/*Checking response */
				if (response != null) {
					numOfConnectTimeoutException = 0;
					InputStream in = response.getEntity().getContent();
					BufferedReader br = new BufferedReader(new InputStreamReader(in));
					// {"data":1454025299000,
					// "progressivoGiornaliero":11,
					// "massimoProgressivoGiornaliero":null,
					// "numeriEstratti":["4","6","8","19","22","35","39","43","48","56","62","66","68","69","75","76","77","83","88","89"],
					// "numeroSpeciale":8}

					while ((read = br.readLine()) != null) {
						if (read.indexOf("Error") > 0) {
							_sendMessage2UI_ERROR_ON_SERVICE(4, "[PG:" + progressivoGiornaliero + "]" + read);
							break; // skip current
						}
						JSONObject jObject = new JSONObject(read);
						LottoDataItem item = new LottoDataItem();
						long data_millisec = jObject.getLong("data");
						item.data = DateUtils.millisec2StringFormat(data_millisec);
						long start_index = DateUtils.getStartIndexAtDate(data_millisec);
						item.progressivoGiornaliero = jObject.getInt("progressivoGiornaliero");
						item._id = start_index + item.progressivoGiornaliero;
						JSONArray array = jObject.getJSONArray("numeriEstratti");
						for (int i = 0; i < array.length(); i++) {
							item.numeriEstratti[i] = array.getInt(i);
						}
						item.numeroSpeciale = jObject.getInt("numeroSpeciale");
						db.saveEstrazione(item);                        // salvo sul DB
						//					FileManager.getInstance().writeOnFile(item);	// salvo su File
						Log.d(TAG, data + "- LOAD #" + progressivoGiornaliero + " - ITEM:" + item);
						_sendMessage2UI(1, item.toString()); // incrementa il contatore...
						lastItemInserted = item;
						String message = "POST_TASK OK data:" + data + " Progressivo gg:" + progressivoGiornaliero;
						FileManager.getInstance().log2File(context, message,"PostTask.log");
						is_data_available = true;
					}
				}
			}
			catch (ConnectTimeoutException e) {
				numOfConnectTimeoutException++;
				Log.d(TAG, progressivoGiornaliero + ":ConnectTimeoutException:" + e.toString());
				//_sendMessage2UI(3,progressivoGiornaliero+":ConnectTimeoutException:"+e.toString());
				//_postJson(data, progressivoGiornaliero, false, ++numRetry);
				String message = "ERROR TIMEOUT - POST_TASK data:" + data + " Progressivo gg:" + progressivoGiornaliero;
				FileManager.getInstance().log2File(context, message,"PostTask.log");
				is_data_available = false;
			}
			catch (Exception e) {
				e.printStackTrace();
				//_sendMessage2UI(4,e.toString());
				//_postJson(data, progressivoGiornaliero, false, ++numRetry);
				Log.d(TAG, e.toString());
				String message = "ERROR - POST_TASK data:" + data + " Progressivo gg:" + progressivoGiornaliero + " - "+e.getMessage();
				FileManager.getInstance().log2File(context, message,"PostTask.log");
				is_data_available = false;
			}
		}
		return is_data_available;
	}

	private HttpClient _createHttpClient(){
		HttpParams params = new BasicHttpParams();
		HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setContentCharset(params, HTTP.DEFAULT_CONTENT_CHARSET);
		HttpProtocolParams.setUseExpectContinue(params, false);

		SchemeRegistry schReg = new SchemeRegistry();
//		schReg.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
		schReg.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
		ClientConnectionManager conMgr = new ThreadSafeClientConnManager(params, schReg);
		return new DefaultHttpClient(conMgr, params);
	}

	private void _sendMessage2UI(int code, String msg) {
		if(notify2Activity){
			Message message = DieciELottoActivity.messageHandler.obtainMessage(code,msg);
			message.sendToTarget();
		}
	}

	private void _sendMessage2UI_ERROR_ON_SERVICE(int code, String msg) {
		Message message = DieciELottoActivity.messageHandler.obtainMessage(code,msg);
		message.sendToTarget();
	}

	/***
	 * Scarica l'ultima estrazione disponibile in base all'ora ed aL giorno
	 * ATTENZIONE: nel caso fosse mezzanotte e' l'estrazione n.288 ma essendo mezzanotte siamo al giorno dopo!
	 * @param context
	 * @param numEstrazione
	 * @param flgUltimaEstrazione
	 * @param flgNotify
	 * @return
	 */
	public static LottoDataItem scaricaEstrazioneNumero(Context context, Calendar startingDay, Integer numEstrazione, boolean flgUltimaEstrazione, boolean flgNotify){
		if(flgUltimaEstrazione){
			startingDay = Calendar.getInstance();
			if(numEstrazione == null) {
				numEstrazione = DateUtils.getNumeroEstrazioniDelGiorno(null);
			}
			if(numEstrazione == 288){
				Date yesterday = DateUtils.addToDate(startingDay.getTime(), -1); // devo recuperare l'estrazione 288 del giorno precedente(siamo appena oltre la mezzanotte!)
				startingDay = DateUtils.date2Calendar(yesterday);
			}
		}
		PostTask task = new PostTask(context);
		task.init(startingDay, numEstrazione, flgNotify);
		Thread tpe = new Thread(task);
		tpe.setDaemon(true);
		tpe.start();
		try {
			FileManager.getInstance().log2File(context, "POST_TASK n.estr:"+numEstrazione,"PostTask.log");
			tpe.join(); // resta in attesa che il TASK termini...
		}
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return task.lastItemInserted;
	}
}