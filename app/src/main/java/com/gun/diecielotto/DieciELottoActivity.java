package com.gun.diecielotto;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.gun.diecielotto.db.DbManager;
import com.gun.diecielotto.db.GiocateLottoDataItem;
import com.gun.diecielotto.db.LottoDataItem;
import com.gun.diecielotto.db.LottoDbHelper;
import com.gun.diecielotto.engine.DataPrediction;
import com.gun.diecielotto.engine.ManageAutoPlay;
import com.gun.diecielotto.engine.PostTask;
import com.gun.diecielotto.service.BroadcastObservable;
import com.gun.diecielotto.service.VerificaGiocateService;
import com.gun.diecielotto.util.DateUtils;
import com.gun.diecielotto.util.GamesUtil;
import com.gun.diecielotto.view.AutomaticPlayFragment;
import com.gun.diecielotto.view.DieciELottoFragment;
import com.gun.diecielotto.view.SelectNumbersToPlayFragment;
import com.gun.diecielotto.view.ShowGamesFragment;
import com.gun.diecielotto.view.ShowPredictionFragment;
import com.gun.diecielotto.view.ShowPredictionGridFragment;

public class DieciELottoActivity extends Activity implements Observer {
	final static String TAG = "DieciELottoActivity";
	static public Handler messageHandler;
	static public Calendar startDay = null;
	private long TIME_TO_SLEEP = 1000;
	private PopupWindow popupHelpWindow = null;

	ProgressDialog progressBarDialog = null;
	CountDownTimer cdt = null;
	DieciELottoFragment delf = null;
	ShowGamesFragment sgf = null;
	ShowPredictionFragment spf = null;
	ShowPredictionGridFragment spgf = null;
	SelectNumbersToPlayFragment sntpf = null;
	AutomaticPlayFragment apf = null;
	DisplayMetrics dm = null;
	public HashSet<Integer> listaDaGiocare = new HashSet<Integer>();
	int count = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.diecielotto_activity);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		dm = getResources().getDisplayMetrics();
		delf = new DieciELottoFragment();
		FragmentTransaction transaction = this.getFragmentManager().beginTransaction();
		transaction.replace(R.id.contenuto_activity, delf);
		transaction.commit();

		BroadcastObservable.getInstance().addObserver(this);
		messageHandler = new Handler(Looper.getMainLooper()) {
			@Override
			public void handleMessage(Message message) {
				switch (message.what) {
				case 0:
					progressBarDialog.show();
					break;

				case 1:
					progressBarDialog.setProgress(count++);
					if (TIME_TO_SLEEP < 1000) {
						TIME_TO_SLEEP = 1000;
					} 
					else {
						TIME_TO_SLEEP -= 100;
					}
					// Log.d(TAG, "==== TEMPO CONNESSIONE ==== :" + TIME_TO_SLEEP);
					// _showMessage(TIME_TO_SLEEP+"...diminuzione tempo attesa",false);
					break;

				case 2:
					if (TIME_TO_SLEEP > 2000) {
						TIME_TO_SLEEP = 2000;
					} 
					else {
						TIME_TO_SLEEP += 100;
					}
					Log.d(TAG, "==== TEMPO ATTESA ==== :" + TIME_TO_SLEEP);
					break;

				case 3:
					progressBarDialog.setProgress(count++);
					break;

				case 4:
					String msg = (String) message.obj;
					msg = msg.length() > 100 ? msg.substring(0, 20) + "...": msg;
					_showMessage("ATTENZIONE:connessione rifiutata dal server..." + msg, false);
				default:
					break;
				}
			}
		};
	}

	@Override
	public void onAttachedToWindow() {
//		Intent intentService = new Intent(this, VerificaGiocateService.class);
//		this.startService(intentService);
		Intent verificaGiocateService = new Intent(this, VerificaGiocateService.class);
		VerificaGiocateService.enqueueWork(this, verificaGiocateService);
	}

	private void _initProgressBar() {
		progressBarDialog = new ProgressDialog(this);
		progressBarDialog.setTitle("synch db");
		progressBarDialog.setCancelable(false);
		progressBarDialog.setMessage("File downloading ...");
		progressBarDialog.setProgress(0);
		progressBarDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	}

	/***
	 * Apre il menu per l'HELP
	 * @param v
	 */
	public void buttonOpenPopupHelp(View v) {
		LayoutInflater inflater = (LayoutInflater) this
				.getSystemService(LAYOUT_INFLATER_SERVICE);

		if (popupHelpWindow == null) {
			View helpView = inflater.inflate(R.layout.help_main, null);
			popupHelpWindow = new PopupWindow(helpView, GamesUtil.getInstance(this).dpToPx(600), GamesUtil.getInstance(this).dpToPx(250));
			if (Build.VERSION.SDK_INT >= 21) {
				// popupHelpWindow.setElevation(5.0f);
			}
			Button closeButton = (Button) helpView.findViewById(R.id.button_close);

			// Set a click listener for the popup window close button
			closeButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					// Dismiss the popup window
					popupHelpWindow.dismiss();
					popupHelpWindow = null;
				}
			});
			popupHelpWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
		}
	}

	/***
	 * Pulsante QUERY schermata principale
	 * @param v
	 */
	public void buttonQueryDB(View v) {
		readDateFromDatePicker();
		int numEstrazione = delf.numberPicker.getValue();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.ITALY);
		LottoDataItem item = DbManager.getInstance(this).find(
				sdf.format(startDay.getTime()), numEstrazione);
		if (null != item) {
			_showMessage("EXT:" + item.toString(), false);
		}
		else {
			_showMessage("Nessuna estrazione per oggi", false);
		}
	}

	@Deprecated
	public void buttonDropDB(View v) {
		boolean res = DbManager.getInstance(this).drop();
		if (res) {
			_showMessage("Rimozione DB", false);
		}
	}

	public void buttonExportDB(View v) {
		DbManager.getInstance(this).exportDatabase(LottoDbHelper.DATABASE_NAME);
	}

	public void buttonImportDBFromAssets(View v) {
		DbManager.getInstance(this).importDatabase(LottoDbHelper.DATABASE_NAME);
	}

	public void buttonSingoliShowListTopDown(View v) {
		spf.buttonSingoliShowListTopDown();
	}

	public void buttonAmbiShowListTopDown(View v) {
		spf.buttonAmbiShowListTopDown();
	}

	/***
	 * Primo metodo di statistiche.
	 * PRONOSTICO 1
	 * @param v
	 */
	@SuppressLint("ResourceType")
	public void buttonForecast(View v) {
		flgCalc = true;
		EditText textStat = (EditText) findViewById(R.id.numero_estrazioni_statistiche);
		int extStat = Integer.parseInt(textStat.getText().toString().trim());
		GamesUtil.getInstance(this).setExtStat(extStat);
		EditText textDelta = (EditText) findViewById(R.id.numero_delta);
		int extDelta = Integer.parseInt(textDelta.getText().toString().trim());
		GamesUtil.getInstance(this).setExtDelta(extDelta);
		readDateFromDatePicker();

		spf = ShowPredictionFragment.getInstance();

		FragmentTransaction transaction = this.getFragmentManager().beginTransaction();
		int width = dm.widthPixels;
		switch (width) {
		case 1280:
			transaction.setCustomAnimations(R.anim.enter_anim_1280,
					R.anim.exit_anim_1280, R.anim.enter_anim_2_1280,
					R.anim.exit_anim_2_1280);
			break;
		case 1600:
			transaction.setCustomAnimations(R.anim.enter_anim_1600,
					R.anim.exit_anim_1600, R.anim.enter_anim_2_1600,
					R.anim.exit_anim_2_1600);
			break;
		default:
			transaction.setCustomAnimations(R.anim.enter_anim_1280,
					R.anim.exit_anim_1280, R.anim.enter_anim_2_1280,
					R.anim.exit_anim_2_1280);
			break;
		}
		transaction.replace(R.id.contenuto_activity, spf, "show_forecast");
		transaction.addToBackStack("show_forecast");
		transaction.commit();
		getFragmentManager().executePendingTransactions();
	}

	/***
	 * Secondo metodo di statistiche. Considera i numeri non estratti nelle
	 * ultime N estrazioni
	 * 
	 * @param v
	 */
	@SuppressLint("ResourceType")
	public void buttonForecast2(View v) {
		flgCalc = true;
		EditText textStat = (EditText) findViewById(R.id.numero_estrazioni_statistiche);
		Integer extStat = Integer.parseInt(textStat.getText().toString().trim());
		GamesUtil.getInstance(this).setExtStat(extStat);
		EditText textDelta = (EditText) findViewById(R.id.numero_delta);
		Integer extDelta = Integer.parseInt(textDelta.getText().toString().trim());
		GamesUtil.getInstance(this).setExtDelta(extDelta);

		readDateFromDatePicker();
		LottoDataItem last = DbManager.getInstance(this).getLastExtractionNow();
		if (last != null) {
			spgf = new ShowPredictionGridFragment();
			Bundle args = new Bundle();
			args.putLong("extRif1", last.getIndexExt() - 20);
			args.putLong("extRif2", last.getIndexExt());
			spgf.setArguments(args);

			FragmentTransaction transaction = this.getFragmentManager()
					.beginTransaction();
			int width = dm.widthPixels;
			switch (width) {
			case 1280:
				transaction.setCustomAnimations(R.anim.enter_anim_1280,
												R.anim.exit_anim_1280,
												R.anim.enter_anim_2_1280,
												R.anim.exit_anim_2_1280);
				break;
			case 1600:
				transaction.setCustomAnimations(R.anim.enter_anim_1600,
						R.anim.exit_anim_1600, R.anim.enter_anim_2_1600,
						R.anim.exit_anim_2_1600);
				break;
			default:
				transaction.setCustomAnimations(R.anim.enter_anim_1280,
						R.anim.exit_anim_1280, R.anim.enter_anim_2_1280,
						R.anim.exit_anim_2_1280);
				break;
			}
			transaction.replace(R.id.contenuto_activity, spgf, "show_forecast2");
			transaction.addToBackStack("show_forecast2");
			transaction.commit();
			getFragmentManager().executePendingTransactions();
		}
		else {
			_showMessage("Ultima estrazione non recuperata!", false);
		}
	}

	/***
	 * Giocate automatiche.
	 * Considera vari parametri per recuperare gli ambi piu' o meno estratti
	 * @param
	 */
	@SuppressLint("ResourceType")
	public void buttonForecast3(View v) {
		EditText textStat = (EditText) findViewById(R.id.numero_estrazioni_statistiche);
		Integer extStat = Integer.parseInt(textStat.getText().toString().trim());
		GamesUtil.getInstance(this).setExtStat(extStat);
		EditText textDelta = (EditText) findViewById(R.id.numero_delta);
		Integer extDelta = Integer.parseInt(textDelta.getText().toString().trim());
		GamesUtil.getInstance(this).setExtDelta(extDelta);
		apf = new AutomaticPlayFragment();
		FragmentTransaction transaction = this.getFragmentManager().beginTransaction();
		transaction.setCustomAnimations(R.anim.enter_up_down_1280,
										R.anim.exit_up_down_1280,
										R.anim.enter_down_up_1280,
										R.anim.exit_down_up_1280);
		transaction.replace(R.id.contenuto_activity, apf, "show_forecast3");
		transaction.addToBackStack("show_forecast3");
		transaction.commit();
	}
	long mLastClickTime = 0;
	public boolean flgCalc = true; // serve per ricalcolare le liste degli ambi e terni

	@SuppressLint("ResourceType")
	public void buttonShowGames(View v) {
		Button b = (Button) v.findViewById(R.id.visualizza_giocate);
		String tag = (String) b.getTag();
        flgCalc = tag.equalsIgnoreCase("calculate_on");
		if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
			return;
		}
		mLastClickTime = SystemClock.elapsedRealtime();

		sgf = ShowGamesFragment.getInstance();
		FragmentTransaction transaction = this.getFragmentManager().beginTransaction();
		int width = dm.widthPixels;
		switch (width) {
		case 1280:
			transaction.setCustomAnimations(R.anim.enter_anim_1280,
					R.anim.exit_anim_1280, R.anim.enter_anim_2_1280,
					R.anim.exit_anim_2_1280);
			break;
		case 1600:
			transaction.setCustomAnimations(R.anim.enter_anim_1600,
											R.anim.exit_anim_1600,
											R.anim.enter_anim_2_1600,
											R.anim.exit_anim_2_1600);
			break;
		default:
			transaction.setCustomAnimations(R.anim.enter_anim_1280,
											R.anim.exit_anim_1280,
											R.anim.enter_anim_2_1280,
											R.anim.exit_anim_2_1280);
			break;
		}
		transaction.replace(R.id.contenuto_activity, sgf, "show_games");
		transaction.addToBackStack(null);
		transaction.commit();
	}

	public void deleteAllGames(View v) {
		DbManager.getInstance(this).deleteAllGames();
		sgf = (ShowGamesFragment) this.getFragmentManager().findFragmentByTag("show_games");
		sgf.gameDataAdapter.refreshList();
//		FragmentTransaction transaction = this.getFragmentManager().beginTransaction();
//		transaction.detach(sgf);
//		transaction.attach(sgf);
//		transaction.commit();
	}

	public void deleteTotGiocatiVinti(View v) {
		GamesUtil.getInstance(this).resetAllPreferences();
		sgf = (ShowGamesFragment) this.getFragmentManager().findFragmentByTag("show_games");
		sgf.updateImportiGiocateVincite();
		FragmentTransaction transaction = this.getFragmentManager().beginTransaction();
		transaction.detach(sgf);
		transaction.attach(sgf);
		transaction.commit();
	}

	public void checkVincite(View v) {
		Thread checkVincite = DataPrediction.getThreadVerificaGiocate(this, false);
		checkVincite.start();
		// lo fa dentro al Thread --> ManageAutoPlay.getInstance(this).startAuto(true);
	}

	// gioca tutti i numeri presenti nella selezione.
	public void giocaNumeri(View v) {
		if (this.listaDaGiocare.size() > 0 && this.listaDaGiocare.size() <= 10) {
			int[] ldg = GamesUtil.getInstance(this).integerArray2Primitive(this.listaDaGiocare.toArray(new Integer[0]));
			GiocateLottoDataItem giocata = GamesUtil.getInstance(this).createGame(ldg);
			DbManager.getInstance(this).saveGiocate(giocata);
		}
		else {
			_showMessage("Giocata non valida", false);
		}
	}

	public void gioca1(View v) {
		int[] ldg = GamesUtil.getInstance(this).integerArray2Primitive(this.listaDaGiocare.toArray(new Integer[0]));
		GamesUtil.getInstance(this).calcolaCombinazioni(ldg, 1);
	}

	public void gioca2(View v) {
		int[] ldg = GamesUtil.getInstance(this).integerArray2Primitive(this.listaDaGiocare.toArray(new Integer[0]));
		GamesUtil.getInstance(this).calcolaCombinazioni(ldg, 2);
	}

	public void gioca3(View v) {
		int[] ldg = GamesUtil.getInstance(this).integerArray2Primitive(this.listaDaGiocare.toArray(new Integer[0]));
		GamesUtil.getInstance(this).calcolaCombinazioni(ldg, 3);
	}

	public void gioca4(View v) {
		int[] ldg = GamesUtil.getInstance(this).integerArray2Primitive(this.listaDaGiocare.toArray(new Integer[0]));
		GamesUtil.getInstance(this).calcolaCombinazioni(ldg, 4);
	}

	public void gioca5(View v) {
		int[] ldg = GamesUtil.getInstance(this).integerArray2Primitive(this.listaDaGiocare.toArray(new Integer[0]));
		GamesUtil.getInstance(this).calcolaCombinazioni(ldg, 5);
	}

	public void gioca6(View v) {
		int[] ldg = GamesUtil.getInstance(this).integerArray2Primitive(this.listaDaGiocare.toArray(new Integer[0]));
		GamesUtil.getInstance(this).calcolaCombinazioni(ldg, 6);
	}

	public void gioca7(View v) {
		int[] ldg = GamesUtil.getInstance(this).integerArray2Primitive(this.listaDaGiocare.toArray(new Integer[0]));
		GamesUtil.getInstance(this).calcolaCombinazioni(ldg, 7);
	}

	public void gioca8(View v) {
		int[] ldg = GamesUtil.getInstance(this).integerArray2Primitive(this.listaDaGiocare.toArray(new Integer[0]));
		GamesUtil.getInstance(this).calcolaCombinazioni(ldg, 8);
	}

	public void gioca9(View v) {
		int[] ldg = GamesUtil.getInstance(this).integerArray2Primitive(this.listaDaGiocare.toArray(new Integer[0]));
		GamesUtil.getInstance(this).calcolaCombinazioni(ldg, 9);
	}

	public void gioca10(View v) {
		int[] ldg = GamesUtil.getInstance(this).integerArray2Primitive(this.listaDaGiocare.toArray(new Integer[0]));
		GamesUtil.getInstance(this).calcolaCombinazioni(ldg, 10);
	}

	// Pulsanti presenti nel pannello pronostico2
	public void buttonMoveLeftStartExt(View v) {
		spgf = (ShowPredictionGridFragment) this.getFragmentManager().findFragmentByTag("show_forecast2");
		spgf.updateStartExtOnGrid(-1);
		Log.d(TAG, "buttonMoveLeftStartExt <<<");
	}

	public void buttonMoveRightStartExt(View v) {
		spgf = (ShowPredictionGridFragment) this.getFragmentManager().findFragmentByTag("show_forecast2");
		spgf.updateStartExtOnGrid(+1);
		Log.d(TAG, "buttonMoveRightStartExt <<<");
	}

	public void buttonMoveLeftEndExt(View v) {
		spgf = (ShowPredictionGridFragment) this.getFragmentManager().findFragmentByTag("show_forecast2");
		spgf.updateEndExtOnGrid(-1);
		Log.d(TAG, "buttonMoveLeftEndExt <<<");
	}

	public void buttonMoveRightEndExt(View v) {
		spgf = (ShowPredictionGridFragment) this.getFragmentManager().findFragmentByTag("show_forecast2");
		spgf.updateEndExtOnGrid(+1);
		Log.d(TAG, "buttonMoveRightEndExt <<<");
	}

	/***
	 * Giocate automatiche
	 * 
	 * @param v
	 */
	public void buttonTestAutomaticPlay(View v) {
		Log.d(TAG, "buttonTestAutomaticPlay");
		ManageAutoPlay.getInstance(this).startAuto(true);
	}

	/***
	 * Carica i dati dalla rete, collegandosi al sito del 10elotto
	 * @param
	 */
	public void buttonLoadByParameters(View v) {
		CompoundButton tb = (CompoundButton) findViewById(R.id.toggle_button);
		final boolean buttonToggleValue = tb.isChecked();
		readDateFromDatePicker();
		_initProgressBar();
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			new Thread(new Runnable() {
				public void run() {
					Message message = DieciELottoActivity.messageHandler.obtainMessage(0); // show progress bar
					message.sendToTarget();
					WebServerConnectionTask wsct = new WebServerConnectionTask();
					if (buttonToggleValue) { // carica "solo oggi" del giorno indicato
						progressBarDialog.setMax(DateUtils.getNumeroEstrazioniDelGiorno(startDay));
						wsct.doInBackground(startDay, true, false);
					} 
					else {	// carica "a partire da" del giorno indicato
						progressBarDialog.setMax(DateUtils.getEstrazioniNumGiorni(startDay));
						wsct.doInBackground(startDay, false, false);
					}
				}
			}).start();
			_showMessage("...loading data", false);
		} else {
			_showMessage("Nessuna connessione dati...", false);
		}
	}

	public void buttonLoadSelectedGameDB(View v) {
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		_initProgressBar();
		if (networkInfo != null && networkInfo.isConnected()) {
			Calendar startingDay = ((DieciELottoActivity) this).readDateFromDatePicker();
			LottoDataItem estrazione = PostTask.scaricaEstrazioneNumero(this, startingDay, delf.numberPicker.getValue(), false, true);
			if (estrazione != null) {
				_showMessage("Estrazione selezionata:" + estrazione,false);
			}
		}
		else {
			_showMessage("Nessuna connessione dati...", false);
		}
	}

	/***
	 * LOAD ULTIMA - carica l'ultima estrazione della giornata
	 * @param v
	 */
	public void buttonLoadLastDB(View v) {
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		_initProgressBar();
		if (networkInfo != null && networkInfo.isConnected()) {
			LottoDataItem ultimaEstrazione = PostTask.scaricaEstrazioneNumero(this, null, null,true, true);
			_showMessage("Ultima estrazione:" + ultimaEstrazione, false);
		}
		else {
			_showMessage("Nessuna connessione dati...", false);
		}
	}

	/***
	 * ...si lo so, restituisce un valore static accessibile a tutti! CHEPPALLE
	 * PERO'... :(
	 * 
	 * @return
	 */
	public Calendar readDateFromDatePicker() {
		startDay = Calendar.getInstance();
		startDay.set(Calendar.YEAR, delf.datePicker.getYear());
		startDay.set(Calendar.MONTH, delf.datePicker.getMonth());
		startDay.set(Calendar.DAY_OF_MONTH, delf.datePicker.getDayOfMonth());
		return startDay;
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		_setScreenResolution(this);
		EditText textStat = (EditText) findViewById(R.id.numero_estrazioni_statistiche);
		textStat.setText(""+GamesUtil.getInstance(this).getExtStat());
		
		EditText textDelta = (EditText) findViewById(R.id.numero_delta);
		textDelta.setText(""+GamesUtil.getInstance(this).getExtDelta());
	}

	public static DisplayMetrics metrics = new DisplayMetrics();

	private static void _setScreenResolution(Context context) {
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		display.getMetrics(metrics);
	}

	private void _showMessage(final String message,final boolean flgCloseProgressBarDialog) {
		this.runOnUiThread(new Runnable() {
			public void run() {
				int duration = Toast.LENGTH_SHORT;
				Toast toast = Toast.makeText(DieciELottoActivity.this, message,
						duration);
				toast.show();
				if (flgCloseProgressBarDialog && progressBarDialog != null
						&& progressBarDialog.isShowing()) {
					progressBarDialog.dismiss();
				}
			}
		});
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	/***
	 * Task per scaricare in parallelo i dati dal Server
	 * @author stefano
	 */
	private class WebServerConnectionTask {
		ThreadPoolExecutor tpe = null;
		public int nGiorni = 0;

		/***
		 * data partenza numero di giorni
		 */
		protected void doInBackground(Calendar startingDay, boolean isSoloOggi, boolean lastExtraction) {
			int cpus = Runtime.getRuntime().availableProcessors();
			this.nGiorni = 1;
			if (!DateUtils.isToday(startingDay) && !isSoloOggi) {
				this.nGiorni = DateUtils.getNumGiorni(startingDay) + 1;
			} 
			int MAX_LIMIT = 10;
			String message = null;
			if (lastExtraction) {
				LottoDataItem ultimaEstrazione = PostTask.scaricaEstrazioneNumero(DieciELottoActivity.this, null, null,true, false);
				message = "Last DATA LOADED is " + ultimaEstrazione;
				_showMessage(message, false);
			}
			else {
				for (int j = 1; j <= this.nGiorni; j++) {
					ExecutorService tpe = Executors.newFixedThreadPool(cpus);
					int ultimaEstrazioneDelGiorno = DateUtils.getNumeroEstrazioniDelGiorno(startingDay);
					for (int i = 1; i <= ultimaEstrazioneDelGiorno; i++) {
						PostTask task = new PostTask(DieciELottoActivity.this);
						if (PostTask.numOfConnectTimeoutException > 5) {
							PostTask.numOfConnectTimeoutException = 0;
							count = 0;
							_showMessage(
									"PROBLEMI DI CONNESSIONE AL SERVER - riprovare piu tardi",
									true);
							return;
						}
						task.init(startingDay, i, true);
						tpe.execute(task);
					}
					tpe.shutdown();
					try {
						if (!tpe.awaitTermination(5, TimeUnit.MINUTES)) {
							tpe.shutdownNow();
							Log.d(TAG, "==== IN ATTESA!");
						}
					} catch (InterruptedException e) {
						tpe.shutdownNow();
						// Preserve interrupt status
						Thread.currentThread().interrupt();
					}
					if (startingDay != null) {
						startingDay.add(Calendar.DAY_OF_YEAR, 1);
					}
				}
				Log.d(TAG, "=== FINE ===");
				message = "DATA LOADED!";
			}
			_showMessage(message, true);
			count = 0;
		}
	}

	private CountDownTimer _getTimer(long millisec){
		CountDownTimer cdt = new CountDownTimer(millisec, 1000) {
			final TextView countdownTextView = (TextView) findViewById(R.id.countdown);
			@Override
			public void onTick(long millisUntilFinished) {
				String minuti = String.format("%02d", millisUntilFinished / 60000);
				int secondi = (int) ((millisUntilFinished % 60000) / 1000);
				countdownTextView.setText(minuti + ":" + String.format("%02d", secondi));
			}

			@Override
			public void onFinish() {
				// countdownTextView.setText("restart");
			}
		};
		return cdt;
	}

	@Override
	public void update(Observable observable, Object data) {
		Log.d(TAG, "=== UPDATE TIMER-GAMES ===");
		if (data != null) {
			long millisec = (Long) data;
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (null != cdt) {
						cdt.cancel();
					}
					cdt = _getTimer(millisec);
					cdt.start();
				}
			});
		}
		sgf = (ShowGamesFragment) this.getFragmentManager().findFragmentByTag("show_games");
		if (sgf != null) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					sgf.updateImportiGiocateVincite();
					final FragmentTransaction ft = (DieciELottoActivity.this).getFragmentManager().beginTransaction();
					sgf.gameDataAdapter.refreshList();
//					ft.detach(sgf);
//					ft.attach(sgf);
//					ft.commit();
				}
			});
		}
	}

	// ====================
	// Sezione BADGE - test
	// ====================
	/*
	private static int countBadge = 0;
	public static void incrementBadge(Context context) {
	    String launcherClassName = getLauncherClassName(context);
	    Log.d(TAG, "=== BADGE ===> " + launcherClassName);
	    Log.d(TAG, "=== countBadge ===> " + countBadge);
	    if (launcherClassName == null) {
	        return;
	    }
	    Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
	    intent.putExtra("badge_count", ++countBadge);
	    intent.putExtra("badge_count_package_name", context.getPackageName());
	    intent.putExtra("badge_count_class_name", launcherClassName);
	    context.sendBroadcast(intent);
	    Log.d(TAG, "=== BADGE ===> " + launcherClassName);
	}

	public static String getLauncherClassName(Context context) {
	    PackageManager pm = context.getPackageManager();
	    Intent intent = new Intent(Intent.ACTION_MAIN);
	    intent.addCategory(Intent.CATEGORY_LAUNCHER);

	    List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
	    for (ResolveInfo resolveInfo : resolveInfos) {
	        String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
	        if (pkgName.equalsIgnoreCase(context.getPackageName())) {
				return resolveInfo.activityInfo.name;
	        }
	    }
	    return null;
	}
	*/
}
