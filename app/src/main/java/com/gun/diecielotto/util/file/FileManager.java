package com.gun.diecielotto.util.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.gun.diecielotto.db.LottoDataItem;
import com.gun.diecielotto.util.DateUtils;

public class FileManager {
	private static final String TAG = "FileManager";
	private static FileManager fm = null;
	private FileWriter fw = null;
	private File fOut = null;
	public String absPath = Environment.getExternalStorageDirectory().getAbsolutePath()+File.separator+"10elotto";
    // public String publicPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + File.separator;
    public String publicPath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator +"10elotto" + File.separator;


	private FileManager(){
		fOut = new File(absPath);
		if(!fOut.exists()){
			fOut.mkdirs();
		}
	}

	public static FileManager getInstance(){
		if(null == fm){
			fm = new FileManager();
		}
		return fm;
	}
	
	private void writeHeaderOnFile(){
		LottoDataItem item = new LottoDataItem();
		item.flg_h = true;
		writeOnFile(item);
	}
	
	public void writeOnFile(LottoDataItem item){
		fOut = new File(absPath,"lottoe10.csv");
		try {
			fw = new FileWriter(fOut,true);
			fw.append(item.toString());
			fw.flush();
			fw.close();
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void log2File(Context context, String message, String fileName) {
		try {
			//String path = context.getFilesDir() + File.separator;
			//String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator;
			//String path = Environment.getDataDirectory().getAbsolutePath() + File.separator;
			Log.d(TAG, publicPath + fileName);
			File file = new File(publicPath, fileName);
			if (!file.exists()) {
				file.createNewFile();
			}
			FileOutputStream fileOutputStream = new FileOutputStream(file,true);
			OutputStreamWriter writer = new OutputStreamWriter(fileOutputStream);
			writer.append("[PID:"+android.os.Process.myPid()+"]"+DateUtils.getDate4log()+message+"\n");
			writer.close();
			fileOutputStream.close();
			//outputStream = openFileOutput(fileName,Context.MODE_APPEND);
			//outputStream.write(message.getBytes());
			//outputStream.close();
		}
		catch (IOException e) {
			Log.e(TAG, e.toString());
		}
	}

	public String readFromFile(Context context, String fileName) {
		StringBuilder stringBuilder = new StringBuilder();
		String line;
		BufferedReader in = null;

		try {
			in = new BufferedReader(new FileReader(new File(context.getFilesDir(), fileName)));
			while ((line = in.readLine()) != null) stringBuilder.append(line);

		} catch (FileNotFoundException e) {
			Log.e(TAG, e.toString());
		} catch (IOException e) {
			Log.e(TAG, e.toString());
		}

		return stringBuilder.toString();
	}

}
