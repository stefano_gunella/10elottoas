package com.gun.diecielotto.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import android.annotation.SuppressLint;
import android.util.Log;

import com.gun.diecielotto.db.BaseDataItem;

@SuppressLint("SimpleDateFormat")
public class DateUtils {
	static SimpleDateFormat dateFormat  = new SimpleDateFormat("yyyyMMdd");
	static SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy");

	static public long getNow(){
		Calendar c = Calendar.getInstance();
		return c.getTimeInMillis();
	}

	static public Date getToday(){
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}

	static public boolean isToday(Calendar day){
		Calendar today = Calendar.getInstance();
		int d1 = day.get(Calendar.DAY_OF_YEAR);
		int d2 = today.get(Calendar.DAY_OF_YEAR);
		return d1 == d2;
	}

	/***
	 * Restituisce la data in formato yyyyMMdd a partire dai millisecondi
	 * @param millisec
	 * @return
	 */
	static public String millisec2StringFormat(long millisec){
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(millisec);
		return dateFormat.format(c.getTime());
	}

	static public Date millisec2Date(long millisec){
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(millisec);
		return c.getTime();
	}

	static public String getDate4log(){
		Calendar calendar = Calendar.getInstance();
		int hh = calendar.get(Calendar.HOUR_OF_DAY);
		int mm = calendar.get(Calendar.MINUTE);
		int ss = calendar.get(Calendar.SECOND);
		String result = String.format("[%02d:%02d:%02d]", hh, mm, ss);
		return result;
	}

	static public String getYYYYMMDD2UI(String strDate){
		String result = strDate.substring(6,8)+"/"+strDate.substring(4,6)+"/"+strDate.substring(0,4);
		return result;
	}

	/***
	 * Restituisce la mezzanotte del giorno di oggi
	 * @return
	 */
	static public long getTodayInMillisec(){
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTimeInMillis();
	}

//	static public String timeToStringFormat(String data){
//		Calendar c = Calendar.getInstance();
//		long time = Long.valueOf(data);
//		c.setTimeInMillis(time);
//		return dateFormat.format(c.getTime());
//	}

	static public String dateToStringFormat(Date data){
		Calendar c = Calendar.getInstance();
		long time = data.getTime();
		c.setTimeInMillis(time);
		return dateFormat.format(c.getTime());
	}

	static public String todayToStringFormat(){
		Calendar c = Calendar.getInstance();
		return dateFormat.format(c.getTime());
	}

	static public String dateToUI(Date date){
		return dateFormat2.format(date.getTime());
	}

	static public String dateToStringFormatUI(Date data){
		Calendar c = Calendar.getInstance();
		long time = data.getTime();
		c.setTimeInMillis(time);
		return dateFormat2.format(c.getTime());
	}

	static public Date stringFormatToDate(String data){
		Date date = null;
		try {
			date = dateFormat.parse(data);
		}
		catch (ParseException e) {
			Log.d("DateUtil",e.getMessage());
		}
		return date;
	}

	static public Date addToDate(Date date, int numDays){
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DAY_OF_YEAR, numDays);
		return c.getTime();
	}

	static public Calendar date2Calendar(Date date){
		Calendar cal=Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}

	static public int getNumGiorni(Calendar startingDay) {
		Calendar today = Calendar.getInstance(); // now!
		long diff = today.getTimeInMillis() - startingDay.getTimeInMillis();
		int numeroGiorni = (int)TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
		return numeroGiorni;
	}

	/***
	 * restituisce il progressivo giornaliero.
	 * restituisce le estrazioni del giorno da 1 a 288,
	 * Se siamo a mezzanotte, e' la 288-esima del giorno precedente
	 * @param day
	 * @return
	 */
	static public int getNumeroEstrazioniDelGiorno(Calendar day) {
		int numeroEstrazioni = 288;
		Calendar today = Calendar.getInstance(); // now!
		if(day == null || day.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)){
			int numeroMinutiOggi = today.get(Calendar.HOUR_OF_DAY) * 60 + today.get(Calendar.MINUTE);
			numeroEstrazioni = numeroMinutiOggi/5;
			numeroEstrazioni = numeroEstrazioni == 0 ? 288 : numeroEstrazioni;
		}
		return numeroEstrazioni;
	}

	static public int getEstrazioniNumGiorni(Calendar startingDay) {
		Calendar today = Calendar.getInstance(); // now!
		long diff = today.getTimeInMillis() - startingDay.getTimeInMillis();
		int numeroGiorni = (int)TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
		int numeroMinutiOggi = today.get(Calendar.HOUR_OF_DAY) * 60 + today.get(Calendar.MINUTE);
		int estrazioniGiorni = numeroGiorni * 288 + numeroMinutiOggi/5;
		return estrazioniGiorni;
	}

	/***
	 * Restituisce l'indice della tabella = n. di blocco di 5 minuti
	 * @param millisec
	 * @return
	 */
	public static long getStartIndexAtDate(long millisec) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(millisec);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		long index = c.getTimeInMillis()/1000/60/5;
		return index;
	}

	/***
	 * Deve restituire il tempo in blocchi di 5 minuti + n.estrazione
	 * @return
	 */
	public static long getIndexNow() {
		Calendar c = Calendar.getInstance();
		long index = c.getTimeInMillis()/1000L/60L/5L;
		return index;
	}

	public static long getIdIndex(BaseDataItem item) {
		return item._id;
	}
}