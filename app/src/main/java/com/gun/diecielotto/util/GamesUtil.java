package com.gun.diecielotto.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.gun.diecielotto.db.BaseDataItem;
import com.gun.diecielotto.db.DbManager;
import com.gun.diecielotto.db.GiocateLottoDataItem;
import com.gun.diecielotto.db.LottoDataItem;
import com.gun.diecielotto.engine.data.BaseItems;

public class GamesUtil {
	private static final String TAG = "GamesUtil";
	private static int ID_THRED = 0;
	private Context context = null;
	private SharedPreferences preferences = null;
	private static GamesUtil instance = null;

	private GamesUtil(Context context){
		this.context = context;
		preferences = PreferenceManager.getDefaultSharedPreferences(context);
	}

	public static GamesUtil getInstance(Context context){
		instance = (instance==null) ? new GamesUtil(context) : instance;
		return instance;
	}

	public synchronized int getIdThred(){
		ID_THRED++;
		return ID_THRED;
	}

//	public void arrayToString(int[] array, String separatore) {
//		int[] combinazione = new int[elementi];
//		_calcolaCombinazioniR(datiPartenza, combinazione, elementi, 0, 0);
//	}


	public void calcolaCombinazioni(int[] datiPartenza, int elementi) {
		int[] combinazione = new int[elementi];
		_calcolaCombinazioniR(datiPartenza, combinazione, elementi, 0, 0);
	}

	// Metodo ricorsivo per il calcolo delle combinazioni
	private void _calcolaCombinazioniR(int[] datiPartenza, int[] combinazione,
			int elementi, int livello, int indicePartenza) {
		if (livello == elementi) {
			GiocateLottoDataItem g = GamesUtil.getInstance(context).createGame(combinazione);
			DbManager.getInstance(context).saveGiocate(g);
			return;
		}
		for (int i = indicePartenza; i <= datiPartenza.length - (elementi - livello); i++) {
			combinazione[livello] = datiPartenza[i];
			_calcolaCombinazioniR(datiPartenza, combinazione, elementi, livello + 1, i + 1);
		}
	}
	
	/***
	 * Verifica se l'estrazione corrente ha avuto una vincita
	 * sGiocate contiene l'elenco delle giocate vincenti
	 * @param estrazione
	 * @param sGiocate
	 * @return
	 */
	@SuppressLint("UseSparseArrays")
	public int controllaVincite(LottoDataItem estrazione, Set<GiocateLottoDataItem> sGiocate){
		HashMap<Integer,Integer> numeriEstrattiAll = new HashMap<Integer,Integer>();
		int vincitaTot = 0;
		for (Integer estratto : estrazione.numeriEstratti) {
			numeriEstrattiAll.put(estratto, estratto);
		}
		List<GiocateLottoDataItem> listaGiocate = DbManager.getInstance(context).getListGiocate();
		for (GiocateLottoDataItem giocata : listaGiocate) {
			int indovinati = 0;
			for (Integer giocato : giocata.numeriGiocati) {
				if(giocato>0 && numeriEstrattiAll.get(giocato)!=null){
					indovinati++;
				}
			}
			_checkVincita(indovinati, giocata);
			if(giocata.vincita>0){
				if(giocata.ext_diff == 0){
					giocata.ext_diff = differenzaExt(estrazione,giocata); // dice dopo quante estrazioni ? risultata vincente la prima volta
				}
				if(giocata.is_max_vincita){
					giocata.ext_diff_max_win = differenzaExt(estrazione,giocata);
				}
				vincitaTot += giocata.vincita;
				DbManager.getInstance(context).updateVincita(giocata);
				if(null != sGiocate){
					sGiocate.add(giocata);
				}
			}
		}
		return vincitaTot;
	}

	/***
	 * Differenza di estrazioni tra 2 giocate o estrazioni.
	 * @param item1
	 * @param item2
	 * @return
	 */
	public long differenzaExt(BaseDataItem item1, BaseDataItem item2) {
		long estrazioniDifferenza = Math.abs((item1.getIndexExt()-item1.ext_delta)-(item2.getIndexExt()-item2.ext_delta));
		return estrazioniDifferenza;
	}
	
	private void _checkVincita(int indovinati, GiocateLottoDataItem giocata) {
		int vincita = 0;
		int numeriGiocatiEffettivi = giocata.getTotaleNumeriGiocati();
		Log.d(TAG, "Giocati:"+numeriGiocatiEffettivi+" - Indovinati:"+indovinati);
		switch (numeriGiocatiEffettivi) {
		case 1:
			vincita = indovinati == 1 ? 3:vincita;
			giocata.is_max_vincita = indovinati == 1;
			break;
		case 2:
			vincita = indovinati == 1 ? 0:vincita;
			vincita = indovinati == 2 ? 14:vincita;
			giocata.is_max_vincita = indovinati == 2;
			break;
		case 3:
			vincita = indovinati == 2 ? 2:vincita;
			vincita = indovinati == 3 ? 50:vincita;
			giocata.is_max_vincita = indovinati == 3;
			break;
		case 4:
			vincita = indovinati == 2 ? 1:vincita;
			vincita = indovinati == 3 ? 10:vincita;
			vincita = indovinati == 4 ? 100:vincita;
			giocata.is_max_vincita = indovinati == 4;
			break;
		case 5:
			vincita = indovinati == 2 ? 1:vincita;
			vincita = indovinati == 3 ? 4:vincita;
			vincita = indovinati == 4 ? 15:vincita;
			vincita = indovinati == 5 ? 150:vincita;
			giocata.is_max_vincita = indovinati == 5;
			break;
		case 6:
			vincita = indovinati == 3 ? 2:vincita;
			vincita = indovinati == 4 ? 10:vincita;
			vincita = indovinati == 5 ? 100:vincita;
			vincita = indovinati == 6 ? 1000:vincita;
			giocata.is_max_vincita = indovinati == 6;
			break;
		case 7:
			vincita = indovinati == 0 ? 1:vincita;
			vincita = indovinati == 4 ? 4:vincita;
			vincita = indovinati == 5 ? 40:vincita;
			vincita = indovinati == 6 ? 400:vincita;
			vincita = indovinati == 7 ? 2000:vincita;
			giocata.is_max_vincita = indovinati == 7;
			break;
		case 8:
			vincita = indovinati == 0 ? 1:vincita;
			vincita = indovinati == 5 ? 20:vincita;
			vincita = indovinati == 6 ? 200:vincita;
			vincita = indovinati == 7 ? 1000:vincita;
			vincita = indovinati == 8 ? 10000:vincita;
			giocata.is_max_vincita = indovinati == 8;
			break;
		case 9:
			vincita = indovinati == 0 ? 2:vincita;
			vincita = indovinati == 5 ? 10:vincita;
			vincita = indovinati == 6 ? 40:vincita;
			vincita = indovinati == 7 ? 400:vincita;
			vincita = indovinati == 8 ? 2000:vincita;
			vincita = indovinati == 9 ? 100000:vincita;
			giocata.is_max_vincita = indovinati == 9;
			break;
		case 10:
			vincita = indovinati == 0 ? 2:vincita;
			vincita = indovinati == 5 ? 5:vincita;
			vincita = indovinati == 6 ? 15:vincita;
			vincita = indovinati == 7 ? 150:vincita;
			vincita = indovinati == 8 ? 1000:vincita;
			vincita = indovinati == 9 ? 20000:vincita;
			vincita = indovinati == 10 ? 1000000:vincita;
			giocata.is_max_vincita = indovinati == 10;
			break;
		}
		giocata.vincita = vincita;
		giocata.vincitaTot += vincita;
	}
	
	public void updateTotaleVincita(int ultimaVincita){
		int importo = preferences.getInt("totaleVincita",0) + ultimaVincita;
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt("totaleVincita",importo);
		editor.commit();
	}

	public void setTotaleVincita(int totaleVincita){
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt("totaleVincita",totaleVincita);
		editor.commit();
	}

	public int getTotaleVincita(){
		return preferences.getInt("totaleVincita",0);
	}
	
	public void updateImportoDaGiocare(int importoGiocato){
		int importo = preferences.getInt("importoDaGiocare",0) - importoGiocato;
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt("importoDaGiocare",importo);
		editor.commit();
	}

	public void updateTotaleGiocate(int importoGiocato){
		int importo = preferences.getInt("totaleGiocate",0) + importoGiocato;
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt("totaleGiocate",importo);
		editor.commit();
	}

	public void setImportoDaGiocare(int importoDaGiocare){
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt("importoDaGiocare",importoDaGiocare);
		editor.commit();
	}
	
	public int getImportoDaGiocare(){
		return preferences.getInt("importoDaGiocare",0);
	}
	
	public void setTotaleGiocate(int totaleGiocate){
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt("totaleGiocate",totaleGiocate);
		editor.commit();
	}
	
	public int getTotaleGiocate(){
		return preferences.getInt("totaleGiocate",0);
	}
	
	public int getTotaleRicavato(){
		return getTotaleVincita()-getTotaleGiocate();
	}
	
	public int getExtStat(){
		return preferences.getInt("extStat", 10);
	}

	// campo "Estrazioni" sul pannella principale
	public void setExtStat(int value){
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt("extStat",value);
		editor.commit();
	}

	public int getExtDelta(){
		return preferences.getInt("extDelta", 0);
	}

	public void setExtDelta(int value){
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt("extDelta",value);
		editor.commit();
	}

	public int getNumMaxExt(){
		return preferences.getInt("num_max_ext", 0);
	}

	public boolean setNumMaxExt(int value){
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt("num_max_ext",value);
		return editor.commit();
	}

	
	public int getNGiocateValide(){
		return preferences.getInt("n_giocate_valide", 0);
	}
	
	public GiocateLottoDataItem createGame(int[] ldg){
		if(ldg == null || ldg.length > 10){
			return null; // troppi numeri da giocare o nessuno
		}
		GiocateLottoDataItem giocata = new GiocateLottoDataItem();
		// DEVONO ESSERE SEMPRE 10
		for (int i=0;i<ldg.length;i++) {
			giocata.numeriGiocati[i]=ldg[i];
		}
		giocata.data = DateUtils.todayToStringFormat();	// data in cui avviene la giocata
		giocata.id_ext = DateUtils.getIndexNow();		// indice dell'estrazione per cui la giocata sar? valida
		giocata.numeroSpeciale = 0;						// per ora lasciamo perdere
		giocata.extStat  = this.getExtStat();			// numero di giocate su cui calcolare le statistiche
		giocata.ext_delta = this.getExtDelta();			// numero di giocate da saltare.
		giocata.progressivoGiornaliero = DateUtils.getNumeroEstrazioniDelGiorno(null)+1;// ? valido dalla prossima giocata :)
		giocata.n_giocate_valide  = this.getNGiocateValide();// numero di giocate valide
		return giocata;
	}

	
	public int[] integerArray2Primitive(Integer[] itemArray){
		int[] result = new int[itemArray.length];
		for (int i = 0; i < itemArray.length; i++) {
			result[i]=itemArray[i];
		}
		return result;
	}
	
	public int[] integerList2Primitive(List<Integer> listInteger){
		int[] result = new int[listInteger.size()];
		for (int i = 0; i < listInteger.size(); i++) {
			result[i]=listInteger.get(i);
		}
		return result;
	}
	
	public List<Integer> add2ListNoDuplicate(List<Integer> listTarget,List<Integer> listSource){
		for (Integer item : listSource) {
			if(!listTarget.contains(item)){
				listTarget.add(item);
			}
		}
		return listTarget;
	}

	public List<Integer> add2ListNoDuplicate(List<Integer> listTarget,int[] aSource){
		for (int item : aSource) {
			if(!listTarget.contains(item)){
				listTarget.add(item);
			}
		}
		return listTarget;
	}

	public int[] listBaseItemToArray(List<BaseItems> listaDaGiocare) {
		ArrayList<Integer> list2Play = new ArrayList<Integer>();
		for (BaseItems item:listaDaGiocare) {
			add2ListNoDuplicate(list2Play,item.ext);
		}
		int[] game = GamesUtil.getInstance(context).integerArray2Primitive(list2Play.toArray(new Integer[0]));
		return game;
	}

	public String listBaseItemToString(List<BaseItems> listaDaGiocare) {
		int[] listInt = listBaseItemToArray(listaDaGiocare);
		StringBuffer result = new StringBuffer();
		for (int item:listInt) {
			result.append(","+item);
		}
		return result.substring(1);
	}

	public void resetAllPreferences(){
		setImportoDaGiocare(0);
		setTotaleGiocate(0);
		setTotaleVincita(0);
	}
	
	public int dpToPx(int dp) {
	    float density = context.getResources()
	                           .getDisplayMetrics()
	                           .density;
	    return Math.round((float) dp * density);
	}
	
	public String getChkSelected(String value){
		return preferences.getString("sp_numeri_da_giocare", value);
	}

	public boolean setChkSelected(String value){
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString("sp_numeri_da_giocare",value);
		return editor.commit();
	}
	
	public boolean isChkSelected(int n){
		String txt = preferences.getString("sp_numeri_da_giocare", "3");
		Log.d(TAG, "NOW checked:"+txt);
		String[] split = txt.split("\\|");
		boolean res = false;
		for (String item : split) {
			if(item.equals(""+n)){
				Log.d(TAG, n+" IS checked:"+txt);
				res = true;
				break;
			}
        }
		Log.d(TAG, n+" IS out:"+res);
		return res;
	}

	public void updateChkPreferences(String txt, boolean is_selected) {
		Log.d(TAG, "-->"+txt+"="+is_selected);
		StringBuilder result = new StringBuilder();
		String temp = null;
		for (int i = 0; i<10 ; i++) {
			temp = (isChkSelected(i+1) && ( !txt.equals(""+(i+1)))) || (is_selected && ( txt.equals(""+(i+1)))) ? ""+(i+1):"";
			result.append("|"+temp);
		}
		Log.d(TAG, "UPD:"+result.toString());
		setChkSelected(result.toString());
	}
}