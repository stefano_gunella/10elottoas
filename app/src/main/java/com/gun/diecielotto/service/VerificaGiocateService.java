package com.gun.diecielotto.service;

import android.app.AlarmManager;
import android.app.Application;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import com.gun.diecielotto.db.GiocateLottoDataItem;
import com.gun.diecielotto.engine.DataPrediction;
import com.gun.diecielotto.util.DateUtils;
import com.gun.diecielotto.util.GamesUtil;
import com.gun.diecielotto.util.file.FileManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.util.Calendar;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

/***
 * @author stefano
 * Servizio di controllo giocate.
 * scarica ogni 5 minuti l'ultima estrazione e la confronta con tutte le giocate in corso.
 */
public class VerificaGiocateService extends JobIntentService {
	private static final String TAG = "VerificaGiocateService";
    private static boolean is_started = false;
    public static final int JOB_ID = 1;
    private NotificationManager notificationManager;
    private String message = null;
    private Context context = null;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
    }

    public static void enqueueWork(Context context, Intent intent) {
        enqueueWork(context, VerificaGiocateService.class, JOB_ID, intent);
    }

	/***
	 * Questo viene rieseguito ogni 5 minuti grazie all'AlarmManager
	 */
    protected void onHandleWork(@NonNull Intent intent) {
        this.context = getApplicationContext();
        Log.d(TAG, "onHandleWork at " + DateUtils.getDate4log());
        initServiceBackground2(VerificaGiocateService.this);
	}

    public void initServiceBackground2(final Context context){
        Log.d(TAG, "initServiceBackground2");
//        Intent intentVerificaGiocate = new Intent(context, VerificaGiocateReceiver.class);
//        intentVerificaGiocate.setAction("INITIALIZED_TIMER");
//        final PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1001, intentVerificaGiocate, PendingIntent.FLAG_UPDATE_CURRENT);
//        Log.d(TAG, "pendingIntent:" + pendingIntent);

        final Calendar calendar = Calendar.getInstance();
        int minutes = calendar.get(Calendar.MINUTE);
        int seconds = calendar.get(Calendar.SECOND);
        int initialDelay = ((minutes % 5) * 60 + seconds) < 10
                ? seconds + 10
                : 300 - ((minutes % 5) * 60 + seconds) + 10;
        calendar.add(Calendar.SECOND, initialDelay);
        BroadcastObservable.getInstance().updateAll(initialDelay * 1000L);// inizializza il timer la prima volta
        Log.d(TAG, "delay:" + initialDelay);

        if(!is_started) {
            String message = "...start Timer";
            FileManager.getInstance().log2File(context, message,"VerificaGiocateService.log");
            Timer t = new Timer();
            t.scheduleAtFixedRate(new TimerTask() {
                                      @Override
                                      public void run() {
                                          try {
                                              int ID_THREAD = GamesUtil.getInstance(context).getIdThred();
                                              String message = "["+ID_THREAD+"]Start scheduler...";
                                              Log.d(TAG, message);
                                              FileManager.getInstance().log2File(context, message,"VerificaGiocateService.log");
                                              // pendingIntent.send();
                                              verificaGiocate();
                                          }
                                          catch (Exception e) {
                                              Log.e(TAG, "Timer update was not canceled. " + e.toString());
                                          }
                                      }

                                      public void verificaGiocate(){
                                          Log.d(TAG, "verificaGiocate");
                                          // Do the work that requires your app to keep the CPU running.
                                          String message = "ID  "+ DateUtils.getIndexNow() + " - Estrazione #" + DateUtils.getNumeroEstrazioniDelGiorno(Calendar.getInstance());
                                          FileManager.getInstance().log2File(context, message,"TaskCheckService.log");
                                          Thread checkVincite = DataPrediction.getThreadVerificaGiocate(context, true);
                                          checkVincite.start();
                                          try {
                                              checkVincite.join();
                                          }
                                          catch (InterruptedException e) {
                                              e.printStackTrace();
                                          }
                                      }
                                  },
                    //Set how long before to start calling the TimerTask (in milliseconds)
                    initialDelay * 1000L,
                    //Set the amount of time between each execution (in milliseconds)
                    5L * 60L * 1000L);

    /*  --- Tentativo di schedulare ogni 5 minuti...non sembra funzionare
            int ID_THREAD = GamesUtil.getInstance(context).getIdThred();
            message = "["+ID_THREAD+"]Start scheduler...";
            Log.d(TAG, message);
            FileManager.getInstance().log2File(context, message,"VerificaGiocateService.log");

            AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                    calendar.getTimeInMillis(),
                    5 * 60 * 1000, // tolgo 5 secondi...non sembra partire giusto
                    pendingIntent);
     */
            is_started = true;
        }
    }



    @Override
    public void onDestroy() {
        final Calendar calendar = Calendar.getInstance();
        String message = "RIAVVIO SERVIZIO";
        Log.d(TAG, message);
        FileManager.getInstance().log2File(this, message,"VerificaGiocateService.log");
        startService(new Intent(this, VerificaGiocateService.class));
    }

}