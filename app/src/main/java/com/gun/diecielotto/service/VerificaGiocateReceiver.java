package com.gun.diecielotto.service;

import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.gun.diecielotto.engine.DataPrediction;
import com.gun.diecielotto.util.DateUtils;
import com.gun.diecielotto.util.file.FileManager;

import java.util.Calendar;

/***
 * @author stefano
 * Servizio di controllo giocate.
 * scarica ogni 5 minuti l'ultima estrazione e la confronta con tutte le giocate in corso.
 */
@Deprecated
public class VerificaGiocateReceiver extends BroadcastReceiver {
	private static final String TAG = "VerificaGiocateReceiver";
	private String message = null;

	/***
	 * Questo viene rieseguito ogni 5 minuti grazie all'AlarmManager
	 */
	@Override
	public void onReceive(final Context context, Intent intent){
		Log.d(TAG, "VerificaGiocateReceiver");
		message = "RECEIVER onReceive";
		Bundle extras = intent.getExtras();
		// Do the work that requires your app to keep the CPU running.
		message = "ID  "+ DateUtils.getIndexNow() + " - Estrazione #" + DateUtils.getNumeroEstrazioniDelGiorno(Calendar.getInstance());
		FileManager.getInstance().log2File(context, message,"TaskCheckService.log");
		Thread checkVincite = DataPrediction.getThreadVerificaGiocate(context, true);
		checkVincite.start();
		try {
			checkVincite.join();
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		// VerificaGiocateReceiver.completeWakefulIntent(intent);

	}
}