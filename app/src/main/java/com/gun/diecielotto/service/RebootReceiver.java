package com.gun.diecielotto.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/***
 * @author stefano
 * Servizio di controllo giocate.
 * scarica ogni 5 minuti l'ultima estrazione e la confronta con tutte le giocate in corso.
 */
public class RebootReceiver extends BroadcastReceiver {
	private static final String TAG = "RebootReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "onReceive");
		Intent intentService = new Intent(context, VerificaGiocateService.class);
		context.startService(intentService);
	}
}