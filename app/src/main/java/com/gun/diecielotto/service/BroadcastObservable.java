package com.gun.diecielotto.service;

import java.util.Observable;

public class BroadcastObservable extends Observable {
	private static BroadcastObservable instance = new BroadcastObservable();

    public static BroadcastObservable getInstance() {
        return instance;
    }

    private BroadcastObservable() {
    }
    
    public void updateAll(Long time) {
        this.setChanged();
        this.notifyObservers(time);
    }
}